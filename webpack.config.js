const Webpack = require('webpack');
const path = require('path');
const validate = require('webpack-validator');
const merge = require('webpack-merge');
const parts = require('./libs/parts');
const pkg = require('./package.json');

const PATHS = {
    app: path.join(__dirname, 'src', 'js', 'client.js'),
    img: path.join(__dirname, 'src', 'img'),
    build: path.join(__dirname, 'dist'),
    styles: path.join(__dirname, 'src', 'styles'),
    vendor: path.join(__dirname, 'node_modules'),
    src: path.join(__dirname, 'src'),
};

const common = {
    entry: {
        app: PATHS.app,
    },
    output: {
        path: PATHS.build,
        filename: '[name].js'
    },
    node: {
        fs: 'empty'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: PATHS.vendor,
                loader: 'babel',
                query: {
                    presets: ['react', 'es2015', 'stage-0'],
                    plugins: ['react-html-attrs', 'transform-class-properties', 'transform-decorators-legacy']
                }
            }
        ]
    }
};


var config;

// Detect how npm is run and branch based on that
switch(process.env.npm_lifecycle_event) {
    case 'build':
        config = merge(
            common,
            {
                output: {
                    path: PATHS.build,
                    filename: '[name].[hash].js',
                }
            },
            parts.clean(PATHS.build),
            parts.setupJsx(PATHS),
            parts.setupFonts(PATHS),
            parts.setupImages(PATHS),
            parts.extractCSS(PATHS),
            parts.generateFavicons(PATHS),
            parts.minify(),
            parts.injectVersion(pkg.version),
            parts.generateHtml(PATHS)
        );
        break;
    default:
        const devOptions = { host: '127.0.0.1', port: 4000 }
        if (process.env.HOST) {
            devOptions.host = process.env.HOST
        }
        if (process.env.PORT) {
            devOptions.port = process.env.PORT
        }
        config = merge(
            common,
            parts.devServer(PATHS, devOptions),
            {
                devtool: 'eval-source-map'
            },
            parts.setupJsx(PATHS),
            parts.setupFonts(PATHS),
            parts.setupImages(PATHS),
            parts.setupCSS(PATHS),
            parts.generateFavicons(PATHS),
            parts.injectVersion(pkg.version),
            parts.generateHtml(PATHS)
        );
}

module.exports = validate(config);

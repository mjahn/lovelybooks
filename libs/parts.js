const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const NpmInstallPlugin = require('npm-install-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const SpritesmithPlugin = require('webpack-spritesmith');

const sassLoaders = [
  'css-loader',
  'postcss-loader',
  'sass-loader'
]

exports.generateHtml = function(paths) {
    return {
        plugins: [
            new HtmlWebpackPlugin({
                title: 'Lovelybooks',
                inject: 'true',
            }),
        ]
    }
}

exports.devServer = function(paths, options) {
    return {
        watchOptions: {
            // Delay the rebuild after the first change
            aggregateTimeout: 300,
            // Poll using interval (in ms, accepts boolean too)
            poll: 1000
        },
        devServer: {
            // Enable history API fallback so HTML5 History API based
            // routing works. This is a good default that will come
            // in handy in more complicated setups.
            historyApiFallback: true,

            // Unlike the cli flag, this doesn't set
            // HotModuleReplacementPlugin!
            hot: true,
            inline: true,

            // Display only errors to reduce the amount of output.
            stats: 'errors-only',

            // Parse host and port from env to allow customization.
            //
            // If you use Vagrant or Cloud9, set
            // host: options.host || '0.0.0.0';
            //
            // 0.0.0.0 is available to all network devices
            // unlike default `localhost`.
            host: options.host, // Defaults to `localhost`
            port: options.port // Defaults to 8080
        },
        plugins: [
            // Enable multi-pass compilation for enhanced performance
            // in larger projects. Good default.
            new webpack.HotModuleReplacementPlugin({
                multiStep: true
            }),
            new NpmInstallPlugin({
                save: true // --save
            })        ]
    };
}

exports.setupJsx = function(paths) {
    return {
        module: {
            loaders: [
                {
                    test: /\.jsx?$/,
                    exclude: paths.vendor,
                    loader: 'babel',
                    query: {
                        presets: ['react', 'es2015', 'stage-0'],
                        plugins: ['react-html-attrs', 'transform-class-properties', 'transform-decorators-legacy'],
                    }
                }
            ]
        }
    }
}

exports.setupFonts = function(paths) {
    return {
        module: {
            loaders: [
                {
                    test: /\.(eot|svg|ttf|woff(2)?)(\?v=\d+\.\d+\.\d+)?/,
                    loader: 'url?limit=5000&name=img/img-[hash:6].[ext]'
                }
            ]
        }
    }
}

exports.hint = function(paths) {
    return {
        preLoaders: [
            {
                test: /\.jsx?$/,
                loader: ['jshint'],
                // define an include so we check just the files we need
                include: paths.app
            }
        ],
    }
}
exports.setupCSS = function(paths) {
    return {
        module: {
            loaders: [
                {
                    test: /\.scss$/,
                    loader: 'style!css!sass',
                    include: [
                        paths.styles,
                        paths.vendor + '/font-awesome'
                    ]
                }
            ]
        }
    };
}


exports.setupImages = function(paths) {
    return {
        module: {
            loaders: [
                {
                    test: /\.(svg|png|jpg|jpeg|gif)$/,
                    loader: "file?name=img/[name].[hash:6].[ext]",
                }
            ]
        }
    };
}

exports.minify = function() {
    return {
        plugins: [
            new webpack.optimize.DedupePlugin(),
            new webpack.optimize.OccurenceOrderPlugin(),
            new webpack.optimize.UglifyJsPlugin({
                // Don't beautify output (enable for neater output)
                beautify: false,

                // Eliminate comments
                comments: false,

                // Compression specific options
                compress: {
                    warnings: false,

                    // Drop `console` statements
                    drop_console: true
                },

                // Mangling specific options
                mangle: {
                    // Don't mangle $
                    except: ['$'],

                    // Don't care about IE8
                    screw_ie8 : true,

                    // Don't mangle function names
                    keep_fnames: true
                }
            })
        ]
    };
}


exports.setFreeVariable = function(key, value) {
    const env = {};
    env[key] = JSON.stringify(value);

    return {
       plugins: [
            new webpack.DefinePlugin(env)
        ]
    };
}

exports.extractBundle = function(options) {
    const entry = {};
    entry[options.name] = options.entries;

    return {
        // Define an entry point needed for splitting.
        entry: entry,
        plugins: [
            // Extract bundle and manifest files. Manifest is
            // needed for reliable caching.
            new webpack.optimize.CommonsChunkPlugin({
                names: [options.name, 'manifest']
            })
        ]
    };
}

exports.clean = function(path) {
    return {
        plugins: [
            new CleanWebpackPlugin([path], {
                // Without `root` CleanWebpackPlugin won't point to our
                // project and will fail to work.
                root: process.cwd()
            })
        ]
    };
}

exports.extractCSS = function(paths) {
    return {
        module: {
            loaders: [
                {
                    test: /\.scss$/,
                    loader: ExtractTextPlugin.extract('style-loader', sassLoaders.join('!'))
                }
            ]
        },
        plugins: [
            new ExtractTextPlugin('[name].[hash:6].css')
        ],
        postcss: [
            autoprefixer({
                browsers: ['last 2 versions']
            })
        ],
        resolve: {
            extensions: ['', '.js', '.scss'],
            root: [paths.src]
        }
    };
}

exports.generateFavicons = function(paths) {
    return {
        plugins: [
            new FaviconsWebpackPlugin({
                logo: path.resolve(paths.img, 'svg/lovelybooks.svg'),
                prefix: 'img/favicons/',
                persistentCache: true,
                inject: true,
                background: '#ffffff',
                title: 'Lovelybooks Mobile',
                online: true,
                icons: {
                    android: true,
                    appleIcon: true,
                    appleStartup: true,
                    coast: true,
                    favicons: true,
                    firefox: true,
                    opengraph: true,
                    twitter: true,
                    yandex: true,
                    windows: true
                }
            }),
        ]
    }
}

exports.injectVersion = function(version) {
    new webpack.DefinePlugin({
        __VERSION__: JSON.stringify(version)
    })
}
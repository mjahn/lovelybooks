import {
    deepOrange900, deepOrange700,
    amber100,
    grey100, grey300, grey400, grey500,
    white, brown800, fullBlack,
} from 'material-ui/styles/colors';
import { fade } from 'material-ui/utils/colorManipulator';
import spacing from 'material-ui/styles/spacing';

export default {
    spacing: spacing,
    fontFamily: '"Open Sans", sans-serif',
    palette: {
        primary1Color: amber100,
        primary2Color: amber100,
        primary3Color: grey400,
        accent1Color: deepOrange900,
        accent2Color: grey100,
        accent3Color: grey500,
        textColor: brown800,
        secondaryTextColor: fade(brown800, 0.54),
        alternateTextColor: deepOrange900,
        canvasColor: white,
        borderColor: grey300,
        disabledColor: fade(brown800, 0.3),
        pickerHeaderColor: white,
        clockCircleColor: fade(brown800, 0.07),
        shadowColor: fullBlack
    },
    chip: {
        textColor: deepOrange900
    },
    textField: {
        floatingLabelColor: brown800,
        focusColor: brown800,
        textColor: brown800,
        disabledTextColor: brown800,
        backgroundColor: 'rgba(255, 255, 255, 0)',
        hintColor: deepOrange900,
        errorColor: deepOrange900
    },
    raisedButton: {
        primaryTextColor: deepOrange900,
        secondaryTextColor: white
    },
    flatButton: {
        primaryTextColor: deepOrange900,
        secondaryTextColor: white
    }
}
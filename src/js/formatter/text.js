import React from 'react'
import Link from 'react-router/lib/Link'
import TextBox from '../components/TextBox'

export function transformLinks(content) {
	if(!content) {
		return content
	}
	//content.replace('', '')
	//
	content = content.replace(
		/http:\/\/www\.lovelybooks\.de\/gruppe\/(\d+)\/([^\/]+)\/themen\/\?threadid=(\d+)&threadtitle=([\d\w+_\-]+)/,
		function(match, id, name, thread, title) {
			if(!title) {
				return match
			}
			return '<a href="#/group/' + id + '/' + name + '/themen/' + thread + '">' + decodeURI(title).replace(/[\+]+/g, ' ') + '</a>'
		}
	)
	return content
}

export function formatSentences(content) {
	if(!content) {
		return content
	}
	content = content.replace(
		/(\w)(,|\.|\?|!)(\w)/g,
		'$1$2 $3'
	)
	content = content.replace(
		/(\s+)(,|\.|\?|!)(\s+)/g,
		'$2 '
	)
	content = content.replace(
		/\s+(,|\.|\?|!)(\w)/g,
		'$1 $2'
	)
	content = content.replace(
		/(\d+)\s*-\s*(\d+)/g,
		'$1-$2'
	)
	return content
}

export function formatMessage(message, recursion) {
	let content = []
    
	if(!recursion) {
		recursion = 0
	}

	let contentParts = message.split('<p>Ursprüngliche Nachricht:')

    content.push(<TextBox key={'text-'+recursion+'-0'} text={contentParts.shift()} />)

    if(contentParts.length > 0) {
    	contentParts = contentParts.join('<p>Ursprüngliche Nachricht:')
        contentParts = contentParts.split('</p>')
        contentParts = contentParts.filter(entry => (entry.trim() !== ''))
        let metaData = contentParts.shift()
        let matches = /von: ([^<]+)(?:<br\s*\/?>)+an: ([^<]+)(?:<br\s*\/?>)+gesendet am: ([\d\.\s:]+)(?:<br\s*\/?>)+Betreff: ([^<]+)/.exec(metaData)

        content.push(<div key={'text-'+recursion+'-1'} class="message-cite">
        	<span class="message-cite-header">
        		<span class="message-cite-from">von <Link to={'/user/' + matches[1] + '/'}>{matches[1]}</Link> </span> 
        		<span class="message-cite-to">an <Link to={'/user/' + matches[2] + '/'}>{matches[2]}</Link> </span> 
        		<span class="message-cite-date">am {matches[3]} </span> 
        		<span class="message-cite-subject">{matches[4]} </span>
        	</span>
	        {formatMessage(contentParts.join('</p>'), recursion + 1)}
	    </div>)
    }
    return content
}
const monthNamesLong = [
	'Januar',
	'Februar',
	'März',
	'April',
	'Mai',
	'Juni',
	'Juli',
	'August',
	'September',
	'Oktober',
	'November',
	'Dezember'
]
const monthNamesShort = [
	'Jan',
	'Feb',
	'Mär',
	'Apr',
	'Mai',
	'Jun',
	'Jul',
	'Aug',
	'Sep',
	'Okt',
	'Nov',
	'Dez'
]
const dayNamesLong = [
	'Sonntag',
	'Montag',
	'Dienstag',
	'Mittwoch',
	'Donnerstag',
	'Freitag',
	'Samstag'
]
const dayNamesShort = [
	'Sonntag',
	'Montag',
	'Dienstag',
	'Mittwoch',
	'Donnerstag',
	'Freitag',
	'Samstag'
]

function pad(value, padding = '00') {
	return (padding + value).slice(-padding.length)
}

export function formatShortDateTime(value) {
	if(value < 200000000000) {
		value = value * 1000
	}
	const date = new Date(value)
	return date.getHours() + ':' + pad(date.getMinutes(), '00') + ' am ' + date.getDate() + '. ' + monthNamesShort[date.getMonth()] + ' ' + date.getFullYear()
}

export function formatLongDate(value) {
	if(value < 200000000000) {
		value = value * 1000
	}
	const date = new Date(value)
	return date.getDate() + '. ' + monthNamesLong[date.getMonth()] + ' ' + date.getFullYear()
}

export function formatMonthDate(value) {
	if(value < 200000000000) {
		value = value * 1000
	}
	const date = new Date(value)
	return monthNamesLong[date.getMonth()] + ' ' + date.getFullYear()
}

export function shortAlphaDate(value) {
	if(value < 200000000000) {
		value = value * 1000
	}
	const date = new Date(value)
	const ref = new Date()
	ref.setHours(0, 0, 0, 1)
	const diff = (ref - date) / 1000

	if(date.toDateString() === ref.toDateString()) {
		return 'heute'
	}
	if(diff < 50 * 86400) {
		return 'vor ' + Math.ceil(diff / 86400) + ' Tagen'
	}
	return 'vor ' + Math.ceil(diff / (30 * 86400)) + ' Monaten'
	return value
}

export function formatNumericDateTime(value) {
	if(value < 200000000000) {
		value = value * 1000
	}
	return date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes()
}
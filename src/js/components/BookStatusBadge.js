import React from 'react'
import Link from 'react-router/lib/Link'

export default class BookStatusBadge extends React.Component {
    render() {
        const { book } = this.props
        if (!book.lesestatus) {
            return null
        }
        if (book.status === 'Zu lesen begonnen') {
            return <div class="listbook-lesestatus">
                <Link to={'/library/book/' + book.link.split('/autor/').join('') + 'status/' + book.lesestatus}>
                    Begonnen
                </Link>
            </div>
        }
        let pages = null
        if (book.pageCurrent && book.pageTotal) {
            pages = <span class="listbook-lesestatus-pages">{book.pageCurrent} / {book.pageTotal}</span>
        }
        return (
            <div class="listbook-lesestatus">
                {pages}
                <Link to={'/library/book/' + book.link.split('/autor/').join('') + 'status/' + book.lesestatus}>
                    Lesestatus aktualisieren
                </Link>
            </div>
        )
    }
}
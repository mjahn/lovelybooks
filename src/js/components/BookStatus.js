import React from 'react'
import { connect } from "react-redux"
import withRouter from 'react-router/lib/withRouter'
import { fetchBook, fetchBookStatus, updateBookStatus }  from '../actions/library'
import BookDetails from './BookDetails'
import BookStatusForm from './BookStatusForm'
import { shortAlphaDate } from '../formatter/date'
import LinearProgress from 'material-ui/LinearProgress'
import Paper from 'material-ui/Paper'

import Loader from './Loader'

require('../../styles/bookstatus.scss')

@connect(store => {
	return {
		bookstatus: store.bookstatus,
		books: store.books,
		setting: store.setting,
	}
})
class BookStatus extends React.Component {
	constructor() {
		super()
		this.saveStatus = this.saveStatus.bind(this)

		this.state = {
			comment: '',
			page: '',
			status: null,
			isLoading: true
		}
	}
	componentWillMount() {
		const { dispatch } = this.props
    	const book = this.props.books['/autor/' + this.props.params.author + '/' + this.props.params.title + '/']
		if (!book) {
			dispatch(fetchBook( { author: this.props.params.author, title: this.props.params.title } ))
		}
		dispatch(fetchBookStatus( { status: this.props.params.status } ))
	}
	saveStatus(data) {
		const { dispatch } = this.props
		dispatch(updateBookStatus(this.props.params.status, {
			comment: data.comment,
			page: data.page,
			status: data.status
		})).then( () => {
			this.state.comment = ''
			dispatch(fetchBookStatus( { status: this.props.params.status } ))
		})
	}
	removeStatus() {
		const { dispatch } = this.props
		dispatch(removeBookStatus(this.props.params.status, {
			comment: this.state.comment,
			page: this.state.page,
			status: this.state.status
		})).then( () => {

			dispatch(fetchBookStatus( { status: this.props.params.status } ))
		})
	}
    render() {
    	const book = this.props.books['/autor/' + this.props.params.author + '/' + this.props.params.title + '/']
		const status = this.props.bookstatus[this.props.params.status]
		let comments  = null

		if(book === undefined || status === undefined) {
			return <Loader />
		}

		if (status.cover) {
			status.cover = status.cover.split('_m.jpg').join('_xxl.jpg');
			status.cover = status.cover.split('_l.jpg').join('_xxl.jpg');
			status.cover = status.cover.split('_xl.jpg').join('_xxl.jpg');
			status.cover = status.cover.split('_s.jpg').join('_xxl.jpg');
    	}

    	if(status.comments && status.comments.length > 0) {
    		this.state.page = status.comments[0].pageCurrent
    		comments = status.comments.map((comment, index) => {
    			const date = shortAlphaDate(comment.created)
				return (
					<li key={status.comments.length - index}>
						<span class="bookstatus-date">{date}</span>
						<div class="bookstatus-progress">
							<em>Seite {comment.pageCurrent}&thinsp;/&thinsp;{comment.pageTotal}</em>
							<LinearProgress mode="determinate" value={comment.percent} class="bookstatus-progress-bar" />

						</div>
						<Paper style={{ backgroundColor: '#FAF9F7' }} class="bookstatus-comment">
							<div class="bookstatus-defaultComment">{comment.defaultComment}</div>
							{comment.comment}
						</Paper>
					</li>
				);
			})
		}
		this.state.page = (status.comments && status.comments[0] ? status.comments[0].pageCurrent : '0')
        return (
            <section className="bookstatus">
                <BookDetails book={book} disableStatus={true} />
                <h3>Zeitleiste</h3>
                <BookStatusForm status={status} onSave={this.saveStatus}/>
                <ol class="bookstatusComments">
                	{comments}
                </ol>
            </section>
      	)

    }
}

export default withRouter(BookStatus)
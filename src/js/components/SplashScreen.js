import React from 'react'
import away from 'away'

export default class SplashScreen extends React.Component {
	static propTypes = {
		enabled: React.PropTypes.bool,
		timeout: React.PropTypes.number,
		onIdle: React.PropTypes.func,
		onActive: React.PropTypes.func
	}

	static defaultProps = {
		timeout: 10 * 60 * 1000,
		enabled: true,
		onActive: () => {},
		onIdle: () => {}
	}

	constructor(props) {
		super(props)
		this.state = {
			idle: true
		}
	}

	createTimer() {
		this.idleTimer = away({
			timeout: this.props.timeout,
			idle: this.state.idle
		})

		this.idleTimer.on('idle', () => {
			this.props.onIdle()
			this.setState({idle: true})
		})

		this.idleTimer.on('active', () => {
			this.props.onActive()
			this.setState({idle: false})
		})
	}

	destroyTimer() {
		this.idleTimer.stop()
	}

	componentDidMount() {
		if (this.props.enabled) {
			this.createTimer()
		}
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.enabled && !this.props.enabled) {
			this.createtimer()
		} else if (!nextProps.enabled && this.props.enabled) {
			this.destroyTimer()
		}
	}

	componentWillUnMount() {
		this.destroyTimer()
	}

	render() {
		if (!this.props.enabled || !this.state.idle) {
			return null
		} else {
			return (
				<div className="splashscreen" style={{ position: 'absolute', top: 0, left: 0, width: '100vw', height: '100vh', color: 'white', overflow: 'hidden', zIndex: 66666, backgroundImage: '../../img/background.jpg' }}>
					{this.props.children}
				</div>
			)
		}
	}
}

import React from 'react'
import { connect } from "react-redux"

import Badge from 'material-ui/Badge'
import Search from 'material-ui/svg-icons/action/search'
import MailOutline from 'material-ui/svg-icons/communication/mail-outline'

import IsbnSearch from './IsbnSearch'

@connect(store => {
	return {
		unread: store.unread,
	}
})
class NotificationArea extends React.Component {
	constructor() {
		super()
		this.state = {
			counter: 0,
		}
	}
	openIsbnScanner() {

	}
	openSearch() {

	}
	render() {
		let badge = null
		if(this.state.counter > 0) { 
			<Badge badgeContent={this.state.counter} primary={true} style={{ padding: '14px 14px 0px 12px' }}>
				<MailOutline />
			</Badge>
		}
		return <div>
			<Search size={36} />
			<IsbnSearch />
			{badge}
		</div>
	}
}

export default NotificationArea
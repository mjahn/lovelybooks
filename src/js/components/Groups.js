import React from 'react'
import { connect } from "react-redux"
import withRouter from 'react-router/lib/withRouter'
import { fetchGroups }  from '../actions/groups'
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import Avatar from 'material-ui/Avatar'
import TextBox from './TextBox'

@connect(store => {
	return {
		groups: store.groups,
		setting: store.setting,
	}
})
class Groups extends React.Component {
    constructor() {
        super()
        this.state = {
        	isLoading: true
        }
    }
	componentWillMount() {
		const { dispatch } = this.props
		dispatch(fetchGroups())
	}
    render() {
        let { groups } = this.props
        if (!this.props.groups) {
            groups = {}
        }
		groups = Object.keys(groups).map(key => {
            const group = this.props.groups[key]
			return (
				<Card key={key} style={{margin: '1rem 0'}}>
                    <CardHeader title={group.title} style={{fontWeight: 'bold'}} />
                    <CardText>
                        <Avatar src={group.cover} size={100} style={{ float: 'left', margin: '0 1rem 1rem 0' }}/>
                        <TextBox text={group.description} />
                    </CardText>
                    <CardActions style={{ clear: 'left' }}>
                        <RaisedButton label="Öffnen" secondary={true} href={'#' + group.link.replace('gruppe', 'groups') } style={{ float: 'right' }}/>
                        <FlatButton label="Verlassen" primary={true} />
                    </CardActions>
                </Card>
			);
		})
        this.state.isLoading = false
        return (
            <section class="groups">
            	<h2>Meine Gruppen</h2>
        		{groups}
            </section>
      	)

    }
}

export default withRouter(Groups)
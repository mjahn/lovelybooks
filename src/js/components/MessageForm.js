import React from 'react'
import { connect } from "react-redux"
import { createMessage }  from '../actions/messages'
import withRouter from 'react-router/lib/withRouter'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
import SelectField from 'material-ui/SelectField'
import Subheader from 'material-ui/Subheader'
import MenuItem from 'material-ui/MenuItem'

require('../../styles/messageform.scss')

@connect(store => {
	return {
        user: store.user,
        messages: store.messages,
        contacts: store.contacts,
		setting: store.setting,
	}
})
class MessageForm extends React.Component {
    constructor() {
        super()
        this.state = {
            subject: '',
            receipients: '',
            message: '',
            isLoading: false
        }
        this.doSendMessage = this.doSendMessage.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.addFriend = this.addFriend.bind(this)
	}
    doSendMessage() {
        const { dispatch } = this.props
        this.state.isLoading = true
        dispatch(createMessage(this.state.receipients, this.state.subject, this.state.message)).then(function() {
            this.state.isLoading = false
            this.props.router.push('/messages/outbox')
        }.bind(this))
        return false
    }
    handleChange(event) {
        this.state[event.target.name] = event.target.value
        return true
    }
    addFriend(event, index, value) {
        this.state.receipients = this.state.receipients + ',' + value
        return true
    }
    render() {
        if(this.props.params.id && this.props.params.action && this.props.messages[this.props.params.id]) {
            const message = this.props.messages[this.props.params.id]
            if(this.props.params.action === 'reply') {
                this.state.subject = message.subject
                if(this.state.subject.indexOf('Re:') < 0) {
                    this.state.subject = 'Re: ' + this.state.subject
                }
                this.state.receipients = message.sendername
                this.state.message = "\n\nUrsprüngliche Nachricht:\nvon: " + message.sendername + "\nan: " + this.props.user.name + "\ngesendet am: " + message.date + "\nBetreff: " + message.subject + "\n\n" + message.description
            }
            if(this.props.params.action === 'forward') {
                this.state.subject = message.subject
                if(this.state.subject.indexOf('Re:') < 0) {
                    this.state.subject = 'Fw: ' + this.state.subject
                }
                this.state.message = "\n\nUrsprüngliche Nachricht:\nvon: " + message.sendername + "\nan: " + this.props.user.name + "\ngesendet am: " + message.date + "\nBetreff: " + message.subject + "\n\n" + message.description
            }
            this.state.message = this.state.message.replace(/<\/?p>/g, '')
            this.state.message = this.state.message.replace(/<br\s*\/?>/g, "\n")
        }
        if(this.props.params.to) {
            this.state.receipients = this.props.params.to
        }

        let predefinedOptions = []
        if(this.props.user.friends) {
            this.props.user.friends.forEach(friend => {
                predefinedOptions.push(<MenuItem key={friend.name} value={friend.name} primaryText={friend.name} />)
            })
        }
        if(this.props.messagers) {
            this.props.contacts.friends.forEach(contact => {
                predefinedOptions.push(<MenuItem key={contact.name} value={contact.name} primaryText={contact.name} />)
            })
        }

        return (
            <section class="messageform">
                <h2>Nachricht verfassen</h2>
                <div>
                    <TextField underlineStyle={{ borderColor: '#433924' }} onChange={this.handleChange} type="text" name="receipients" hintText="Empfänger auswählen" value={this.state.receipients} fullWidth={true} />
                    <SelectField underlineStyle={{ borderColor: '#433924' }} value={this.state.value} hintText="Freund hinzufügen" onChange={this.addFriend} fullWidth={true} >
                        {predefinedOptions}
                    </SelectField>
                    <TextField underlineStyle={{ borderColor: '#433924' }} onChange={this.handleChange} type="text" name="subject" hintText="Betreff eingeben" value={this.state.subject} fullWidth={true} />
                    <TextField underlineStyle={{ borderColor: '#433924' }} onChange={this.handleChange} type="text" name="message" hintText="Nachricht eingeben" multiLine={true} rows={7} rowsMax={30} value={this.state.message} fullWidth={true} />
                </div>
                <div>
                    <RaisedButton label="Absenden" secondary={true} fullWidth={true} onClick={this.doSendMessage} />
                </div>
            </section>
      	)

    }
}

export default withRouter(MessageForm)
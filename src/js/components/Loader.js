import React from 'react'

import RefreshIndicator from 'material-ui/RefreshIndicator'


export default class Loader extends React.Component {
    render() {
		return <div style={{ position: 'relative' }}>
			<RefreshIndicator size={40} left={'50%'} top={50} status="loading" style={{ display: 'inline-block', position: 'relative' }} />
		</div>
	}
}
import React from 'react'
import { connect } from "react-redux"
import withRouter from 'react-router/lib/withRouter'
import { fetchGroup }  from '../actions/groups'
import Loader from './Loader'
import Link from 'react-router/lib/Link'

require('../../styles/group.scss')

@connect(store => {
	return {
		groups: store.groups,
		setting: store.setting,
	}
})
class GroupForum extends React.Component {
    constructor() {
        super()
        this.state = {
            isLoading: true
        }
    }
    componentWillMount() {
        const { dispatch } = this.props
        dispatch(fetchGroup(this.props.params.id, this.props.params.name)).then(() => {
            this.state.isloading = false
        })
        this.state.isLoading = true
    }
    render() {
        this.state.isLoading = false

        return (
            <section class="groupforum">
            </section>
        )

    }
}

export default withRouter(GroupForum)
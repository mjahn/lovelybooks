import React from 'react'
import { connect } from "react-redux"
import withRouter from 'react-router/lib/withRouter'
import Link from 'react-router/lib/Link'

import TextBox from './TextBox'
import { formatNumericDateTime } from '../formatter/date'
import { formatMessage } from '../formatter/text'
import Loader from './Loader'

import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import Avatar from 'material-ui/Avatar'
import IconButton from 'material-ui/IconButton'
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar'
import ChevronLeft from 'material-ui/svg-icons/navigation/chevron-left'
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right'
import Reply from 'material-ui/svg-icons/content/reply'
import Delete from 'material-ui/svg-icons/action/delete'

require('../../styles/message.scss')

@connect(store => {
	return {
        inbox: store.inbox,
		outbox: store.outbox,
        messages: store.messages,
		setting: store.setting,
	}
})
class Message extends React.Component {
    constructor() {
        super()
        this.state = {
			isLoading: true,
            prevId: 0,
            nextId: 0
		}
        this.loadPrevMessage = this.loadPrevMessage.bind(this)
        this.loadNextMessage = this.loadNextMessage.bind(this)
	}
    loadPrevMessage() {
    	if(this.state.prevId === 0) {
    		return
    	}
        this.props.router.replace('/messages/' + this.props.params.folder + '/' + this.state.prevId)
    }
    loadNextMessage() {
    	if(this.state.nextId === 0) {
    		return
    	}
        this.props.router.replace('/messages/' + this.props.params.folder + '/' + this.state.nextId)
    }
    getPrevLink() {
        if(this.state.prevId === 0) {
            return
        }
        return <Link to={'/messages/' + this.props.params.folder + '/' + this.state.prevId} className="fa fa-chevron-left"></Link>
    }
    getNextLink() {
        if(this.state.nextId === 0) {
            return
        }
        return <Link to={'/messages/' + this.props.params.folder + '/' + this.state.nextId} className="fa fa-chevron-right"></Link>
    }
    componentWillMount() {
        this.state.isLoading = true
        if (!this.props.messages[this.props.params.id]) {
            this.props.router.replace('/messaages/inbox')
        }
    }
    render() {
        if (!this.props.messages[this.props.params.id]) {
            return <Loader />
        }

        const message = this.props.messages[this.props.params.id]
        const date = formatNumericDateTime(message.date)

        const messages = this.props[this.props.params.folder].map(key => this.props.messages[key])
        const id = Number(this.props.params.id)

        this.state.prevId = 0
        this.state.nextId = 0
        messages.sort((a, b) => b.date - a.date )
        messages.forEach(function(message, index) {
            if (message.id !== id) {
                return
            }
            if(messages[index - 1]) {
                this.state.prevId = messages[index - 1].id
            }
            if(messages[index + 1]) {
                this.state.nextId = messages[index + 1].id
            }
        }.bind(this))
        let actions = []
        if (message.folder === 'inbox') {
            actions.push(<RaisedButton key={0} label="Antworten" href={'#/messages/new/reply/' + message.id} primary={true} />)
        }
        actions.push(<FlatButton key={1} label="Löschen" href={'#/messages/remove/' + message.id} secondary={true} />)

        const text = formatMessage(message.description)

        this.state.isLoading = false
        return (
            <section class="message">
                <Toolbar>
                    <ToolbarGroup firstChild={true}>
                        <div style={{ display: 'block' }}>
                            <Link to={'/user/' + message.sendername + '/profile'} style={{ float: 'left' }}><Avatar style={{ margin: '0.5rem' }}src={message.senderimage} /></Link>
                            {message.folder === 'inbox' ? 'von' : 'an'} <Link to={'/user/' + message.sendername + '/profile'}>{message.sendername}</Link>
                            <br />
                            {date}
                        </div>
                    </ToolbarGroup>
                    <ToolbarGroup lastChild={true}>
                        <IconButton href={'#/messages/' + this.props.params.folder + '/' + this.state.prevId}>
                            <ChevronLeft size={24} />
                        </IconButton>
                        <IconButton href={'#/messages/' + this.props.params.folder + '/' + this.state.nextId}>
                            <ChevronRight size={36} />
                        </IconButton>
                    </ToolbarGroup>
                </Toolbar>
                <h2 class="message-subject">{message.subject}</h2>
                {text}
                <div class="message-actions">
                    {actions}
                </div>
            </section>
      	)

    }
}

export default withRouter(Message)
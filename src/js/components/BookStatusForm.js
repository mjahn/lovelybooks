import React from 'react'

import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'

require('../../styles/bookstatusform.scss')

export default class BookStatusForm extends React.Component {
    constructor() {
        super()
        this.handleChange = this.handleChange.bind(this)
        this.saveStatus = this.saveStatus.bind(this)

        this.state = {
            comment: '',
            page: 0,
            status: null,
            isLoading: true
        }
    }
    saveStatus() {
        this.props.onSave({
            comment: this.state.comment,
            page: this.state.page,
            status: this.state.status
        })
    }
    handleChange(event) {
        this.state[event.target.name] = event.target.value
        return true
    }
    render() {
        const styles = {
            errorStyle: {
                color: '#981217',
            },
            underlineStyle: {
                borderColor: '#433924',
            },
            hintStyle: {
                color: '#433924',
            },
        }
        const { status } = this.props
        if(!status) {
            return null
        }
        return (
            <form className="bookstatusForm">
                <div style={{ overflow: 'hidden' }}>
                    <TextField underlineStyle={styles.underlineStyle} onChange={this.handleChange} hintText="Kommentar (optional)" type="text" name="comment" fullWidth={true} multiLine={true} rows={3} rowsMax={7} />
                    <TextField underlineStyle={styles.underlineStyle} onChange={this.handleChange} hintText="Seite" type="number" name="page" fullWidth={false} style={{ width: '6rem', float: 'left' }} />
                    <RaisedButton label="Aktualisieren" secondary={true} onClick={this.saveStatus} style={{ marginTop: '1rem' }} fullWidth={false} style={{ float: 'right' }}  />
                </div>
            </form>
        )
    }
}
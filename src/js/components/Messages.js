import React from 'react'
import { connect } from "react-redux"
import withRouter from 'react-router/lib/withRouter'
import Waypoint from 'react-waypoint'

import Avatar from 'material-ui/Avatar'
import ContentAdd from 'material-ui/svg-icons/content/add';
import Divider from 'material-ui/Divider'
import DropDownMenu from 'material-ui/DropDownMenu';
import FilterList from 'material-ui/svg-icons/content/filter-list'
import FloatingActionButton from 'material-ui/FloatingActionButton';
import IconButton from 'material-ui/IconButton'
import IconMenu from 'material-ui/IconMenu'
import Link from 'react-router/lib/Link'
import MenuItem from 'material-ui/MenuItem'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import Search from 'material-ui/svg-icons/action/search'
import Subheader from 'material-ui/Subheader';
import TextField from 'material-ui/TextField';
import { List, ListItem } from 'material-ui/List'
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar';

import { formatShortDateTime, formatMonthDate } from '../formatter/date'
import { fetchMessages }  from '../actions/messages'
import MessageList from './MessageList'
import TextBox from './TextBox'

require('../../styles/messages.scss')

@connect(store => {
	return {
		messages: store.messages,
		inbox: store.inbox,
		outbox: store.outbox,
		setting: store.setting,
	}
})
class Messages extends React.Component {
    constructor() {
        super()
        this.state = {
        	folders: {
        		inbox: {
        			page: 0,
                    complete: false,
        		},
        		outbox: {
        			page: 0,
                    complete: false,
        		},
        	},
        	sortBy: 'date',
        	searchBy: '',
			currentFolder: 'inbox',
		}
		this.handleChange = this.handleChange.bind(this)
		this.loadNextPage = this.loadNextPage.bind(this)
        this.handleMessageMenu = this.handleMessageMenu.bind(this)
        this.showMessage = this.showMessage.bind(this)
        this.openFilterPopup = this.openFilterPopup.bind(this)
    }
    handleMessageMenu(event, item, value) {
        this.props.router.push('/messages/new/' + item.props.value + '/' + item.props.dataMessageId)
    }
    showMessage(message) {
        return function() {
            this.props.router.push('/messages/' + message.folder + '/' + message.id)
        }.bind(this)
    }
	handleChange(event, item, value) {
        const { dispatch } = this.props
        const state = Object.assign({}, this.state)
        if(event.target.id) {
            state[event.target.id] = item
        } else {
            state.currentFolder = value
            dispatch(fetchMessages(value, 0))
        }
        this.setState(state)
        console.dir(this.state)
	}
    shouldComponentUpdate(nextProps) {
        if(this.props[this.state.currentFolder].length > nextProps[this.state.currentFolder].length) {
            const state = Object.assign({}, this.state)
            state.folders[this.state.currentFolder].complete = true
            this.setState(state)
        }
        if(this.props[this.state.currentFolder].length !== nextProps[this.state.currentFolder].length) {
            return true
        }
        return false
    }
	openFilterPopup() {

	}
    loadNextPage() {
        const { dispatch } = this.props
        if(this.state.folders[this.state.currentFolder].complete) {
            return
        }
        this.state.folders[this.state.currentFolder].page = this.state.folders[this.state.currentFolder].page + 1
        dispatch(fetchMessages(this.state.currentFolder, this.state.folders[this.state.currentFolder].page))
    }
    componentWillMount() {
        const { dispatch } = this.props

        dispatch(fetchMessages(this.state.currentFolder, 0))
    }
    render() {
        const styles = {
            errorStyle: {
                color: '#981217',
            },
            underlineStyle: {
                borderColor: '#433924',
            },
            hintStyle: {
                color: '#433924',
            },
        }
        let messages = this.props[this.state.currentFolder].map(id => this.props.messages[id])
        let oldDate = ''
        let messageList = []
        let lists = []
        const iconButtonElement = (
        <IconButton touch={true} tooltip="more" tooltipPosition="bottom-left">
            <MoreVertIcon color="#981217" />
        </IconButton>
        )
        messages.sort((a, b) => b[this.state.sortBy] - a[this.state.sortBy] )

        messages.forEach((message, index) => {
            const rightIconMenu = (
            <IconMenu iconButtonElement={iconButtonElement} onItemTouchTap={this.handleMessageMenu}>
                <MenuItem key="1" dataMessageId={message.id} value="reply">Antworten</MenuItem>
                <MenuItem key="3" dataMessageId={message.id} value="delete">Löschen</MenuItem>
            </IconMenu>
            )
            const newDate = formatMonthDate(message.date)
            if (oldDate !== newDate) {
                oldDate = newDate
                if(messageList.lenght > 0) {
                    lists.push(<List>{messageList}</List>, <Divider />)
                    messageList = []
                }
                messageList.push(<Subheader inset={true} key={'message-' + message.id + '-date'}>{newDate}</Subheader>)
            }
            let text = ''
            if(message.folder === 'inbox') {
                text = 'von ' + message.sendername + ' um ' + formatShortDateTime(message.date) + ': ' + message.description
            }
            if(message.folder === 'outbox') {
                text = 'an ' + message.sendername + ' um ' + formatShortDateTime(message.date) + ': ' + message.description
            }
            text = text.replace(/<\/?[^>]>/g, '')
            messageList.push(<ListItem onClick={this.showMessage(message)} key={'message-' + message.id} leftAvatar={<Avatar src={message.senderimage} />} primaryText={message.subject} secondaryText={text} secondaryTextLines={2} rightIconButton={rightIconMenu} />)
            if(messages.length - index < 4) {
	           messageList.push(<Waypoint key={'waypoint-' + index} onEnter={this.loadNextPage} />)
            }
        })
        if(messageList.length > 0) {
            lists.push(<List key={'list-' + lists.length}>{messageList}</List>)
        }
        return (
            <section class="messages">
                <Toolbar class="messages-toolbar">
                    <ToolbarGroup firstChild={true}>
                        <DropDownMenu value={this.state.currentFolder} onChange={this.handleChange}>
                            <MenuItem id="currentFolder" value={'inbox'} primaryText="Posteingang" />
                            <MenuItem id="currentFolder" value={'outbox'} primaryText="Postausgang" />
                        </DropDownMenu>
                    </ToolbarGroup>
                    <ToolbarGroup>
                        <TextField id="searchBy" errorStyle={styles.errorStyle} underlineStyle={styles.underlineStyle} hintStyle={styles.hintStyle} onChange={this.handleChange} type="search" name="searchBy" hintText="Suche" defaultValue={this.state.searchBy} fullWidth={true} />
                    </ToolbarGroup>
                    <ToolbarGroup lastChild={true}>
                        <FilterList class="messages-toolbar-action" size={30} onClick={this.openFilterPopup} />
                    </ToolbarGroup>
                </Toolbar>
                {lists}
				<FloatingActionButton href="#/messages/new" style={{position: 'fixed', bottom: '2rem', right: '1rem'}} backgroundColor="#891217" zDepth={4}>
					<ContentAdd />
				</FloatingActionButton>
	        </section>
      	)


    }
}

export default withRouter(Messages)
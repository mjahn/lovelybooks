import React from 'react'
import { connect } from "react-redux"
import withRouter from 'react-router/lib/withRouter'
import Link from 'react-router/lib/Link'

import Avatar from 'material-ui/Avatar'
import Paper from 'material-ui/Paper'
import Chip from 'material-ui/Chip'

import { fetchAuthor }  from '../actions/library'
import Loader from './Loader'
import TextBox from './TextBox'
import BookList from './BookList'

require('../../styles/author.scss')

@connect(store => {
	return {
		authors: store.authors,
        books: store.books,
		setting: store.setting,
	}
})
class Author extends React.Component {
    constructor() {
        super()
        this.state = {
            isLoading: true
        }
    }
    componentWillMount() {
        const { dispatch } = this.props
        dispatch(fetchAuthor(this.props.params.author)).then(() => this.state.isloading = false)
        this.state.isLoading = true
    }
    render() {
        let author = this.props.authors['/autor/' + window.encodeURIComponent(this.props.params.author) + '/']
        if (!author || !author.name) {
            return <Loader />
        }
        let bookList = []
        if(author.books) {
            bookList = author.books.map(bookid => this.props.books[bookid])
        }
        let facts = null
        if(author.facts.length > 0) {
            let factList = []
            Object.keys(author.facts).map(key => factList.push(<dt key={'term- ' + key}>{key}</dt>, <dd key={'definition-' + key} dangerouslySetInnerHTML={{ __html: author.facts[key].replace('<a href="/buecher/', '<a href="#/library/genre/') }}></dd>))
            facts = <Paper key={'author-facts'} class="author-facts"><dl>{factList}</dl></Paper>
        }
        let tags = null
        if(author.tags) {
            tags = author.tags.map((tag, index) => {
                return <Link key={'author-tags-' + index} to={'/library/tag/' + tag + '/'}>
                    <Chip>
                        {tag}
                    </Chip>
                </Link>
            })
            tags = <Paper key={'author-tags'} class="author-tags"><h3>Tags</h3><div class="author-tags-container">{tags}</div></Paper>
        }
        let videos = null
        if(author.videos.length > 0) {
            videos = author.videos.map((video, index) => {
                return <Paper key={'author-videos'} zDepth={2} key={'author-video-' + index} style={{ width: '245px', display: 'inline-block', margin: '0.5rem', padding: 0 }}><div dangerouslySetInnerHTML={{ __html: video.embedd }}></div></Paper>
            })
            videos = <div class="author-videos"><h3>Videos</h3>{videos}</div>
        }

        let biography = []
        if(author.biography) {
            biography.push(<h3 key={'author-biography-0'}>Lebenslauf</h3>, <TextBox key={'author-biography-1'} class="author-biography" text={author.biography} />)
        }

        let about = []
        if(videos || tags) {
            about.push(<h3 key={'author-about-0'}>Über {author.name}</h3>, videos, tags)
        }

        this.state.isLoading = false
        return (
            <section class="author">
                <h2>{author.name}</h2>
                <Avatar size={80} src={author.image} class="author-avatar" />
                {facts}
                {biography}
                {about}
                <h3>Bücher von {author.name}</h3>
                <BookList books={bookList} size="m" />
            </section>
        )

    }
}

export default withRouter(Author)
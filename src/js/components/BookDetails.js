import React from 'react'
import Link from 'react-router/lib/Link'
import Paper from 'material-ui/Paper'
import Done from 'material-ui/svg-icons/action/done'
import ImportContacts from 'material-ui/svg-icons/communication/import-contacts'

require('../../styles/bookdetails.scss')

class BookDetails extends React.Component {
    render() {
    	const book = this.props.book

        if(!book || !book.authorlink || !book.author || !book.title || !book.cover) {
            return null
        }

        if(!book.company) {
            book.company = ''
        }
        if(!book.published) {
            book.published = ''
        }

        const year = (new Date(book.published * 1000)).getFullYear()
        let cover = book.cover.split('_')
        cover.pop()
        cover = cover.join('_') + '_xl.jpg'

        let raffle = []
        if(book.raffle) {
            raffle.push(<Link key="book-raffle" class="bookdetails-raffle" to={book.link.replace('/autor/', '/library/book/') + 'raffle/' + book.raffle.id}>
                <ImportContacts size={12} color={'#ffffff'} style={{ marginRight: '0.5rem', verticalAlign: 'bottom' }} />
                Leserunde besuchen
            </Link>)
        }

        let lesestatus = null
        if(book.lesestatus && !this.props.disableStatus) {
            let pages = null
            if (book.pageCurrent && book.pageTotal) {
                pages = <span class="bookdetails-lesestatus-pages">{book.pageCurrent}&thinsp;/&thinsp;{book.pageTotal}</span>
            } else if (book.percent === '100') {
                pages = <span class="bookdetails-lesestatus-pages"><Done color={'#ffffff'} /></span>
            } else if(book.status) {
                if(book.status === 'Zu lesen begonnen') {
                    pages = 'Begonnen'
                } else {
                    pages = <span class="bookdetails-lesestatus-pages">{book.status.indexOf(' ') > 0 ? book.status.split(' ')[1] + '/' + book.status.split(' ')[3] : book.status}</span>
                }
            }
            if(pages) {
                lesestatus = <Link class="bookdetails-lesestatus" to={book.link.split('/autor/').join('/library/book/') + 'status/' + book.lesestatus}>
                    {pages}
                </Link>
            }
        }

        return (
        	<div class="bookdetails">
                <header>
                    <h1><Link class="bookdetails-author" to={book.authorlink.replace('/autor/', '/library/author/')}>{book.author}</Link></h1>
                    <h2 class="bookdetails-title">{book.title}</h2>
                    <h3 class="bookdetails-year">{book.published ? year + (book.company !== '' ? ', ' : '') : ''}{book.company}</h3>
                </header>
                <Paper class="bookdetails-cover" zDepth={3}>
            	    <img src={cover}/>
                    {lesestatus}
                    {raffle}
                </Paper>
            </div>
      )

    }
}

export default BookDetails
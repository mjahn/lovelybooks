import React from 'react'
import ReactDOM from 'react-dom'
import { connect } from "react-redux"
import { fetchRaffles }  from '../actions/library'


import BookList from './BookList'

require('../../styles/dashboard.scss')

@connect(store => {
	return {
        user: store.user,
		books: store.books,
        raffles: store.raffles,
	}
})
class Dashboard extends React.Component {
    constructor() {
        super()
        this.state = {
            isLoading: true
        }
    }
    componentWillMount() {
        const { dispatch } = this.props
        dispatch(fetchRaffles())
    }
    render() {
    	let { books, user, raffles } = this.props
        if(!user.current) {
            user.current = []
        }
        if(!raffles) {
            raffles = []
        }

        let bookList = user.current.map(id => books[id])
        let raffleList = raffles.map(raffle => {
            const raffleBook = books[raffle.book]
            if(!raffleBook) {
                return null
            }
            raffleBook.raffle = raffle
            return raffleBook
        })
        return (
            <section class="dashboard">
	            <h3>Meine Leseecke</h3>
                <BookList id="current" books={bookList} size="m" />
                <h2>Aktuelle Leserunden</h2>
                <BookList id="raffles" books={raffleList} size="m" />
            </section>
        )

    }
}

export default Dashboard
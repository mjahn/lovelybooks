import React from 'react'
import Avatar from 'material-ui/Avatar'
import Divider from 'material-ui/Divider'
import Link from 'react-router/lib/Link'

require('../../styles/userbadge.scss')

class UserBadge extends React.Component {
    constructor() {
        super()
        this.navigate = this.navigate.bind(this)
    }
    navigate(event) {
        this.props.onItemTouchTap(event, { value: ''});
    }
    render() {
    	const { user } = this.props

    	if(!user.stats) {
    		return null
    	}

    	return (
    		<div class="userbadge">
				<Link onClick={this.navigate} to={'/user/' + user.name + '/profile'} class="username">{user.name}</Link>
				<Link onClick={this.navigate} to={'/user/' + user.name + '/profile'} class="userimage">
    				<Avatar size={60} src={user.image} />
				</Link>
				<p class="userstats">
					{user.stats.groups}&nbsp;Gruppen, {user.stats.books}&nbsp;Bücher, {user.stats.eselsohren}&nbsp;Eselsohren, {user.stats.ratings}&nbsp;Bewertungen (&#216; {user.stats.ratingsAverage}), {user.stats.reviews}&nbsp;Rezensionen
				</p>
    		</div>
    	)
    }
}

export default UserBadge
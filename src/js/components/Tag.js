import React from 'react'
import { connect } from "react-redux"
import withRouter from 'react-router/lib/withRouter'
import Link from 'react-router/lib/Link'

import BookList from './BookList'
import { fetchTagPage } from '../actions/tag'
import Loader from './Loader'


require('../../styles/library.scss')

@connect(store => {
	return {
		tags: store.tags,
        books: store.books,
		setting: store.setting,
	}
})
class Tags extends React.Component {
    constructor() {
        super()
        this.state = {
            page: 0
        }
    }
    componentWillMount() {
        const { dispatch } = this.props
        dispatch(fetchTagPage(this.props.params.tag, 0))
    }
    render() {
        const tag = this.props.params.tag
        const library = this.props.tags[tag]
        if (!library || !library.id) {
            return <Loader />
        }
        let bookList = []
        if(library.books) {
            bookList = library.books.map( bookid => this.props.books[bookid] )
        }

        return (
            <section class="library">
                <h2>Tag <q>{library.id}</q></h2>
                <BookList id={library.id} books={bookList} size="m" />
            </section>
        )

    }
}

export default withRouter(Tags)
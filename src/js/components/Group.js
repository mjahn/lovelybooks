import React from 'react'
import { connect } from "react-redux"
import withRouter from 'react-router/lib/withRouter'
import { fetchGroup }  from '../actions/groups'
import { fetchLibraryPage }  from '../actions/library'
import Link from 'react-router/lib/Link'
import Avatar from 'material-ui/Avatar'
import Divider from 'material-ui/Divider'
import { Card, CardActions, CardHeader, CardText } from 'material-ui/Card'

import { formatLongDate } from '../formatter/date'

import Threads from './Threads'
import BookList from './BookList'
import UserList from './UserList'
import TextBox from './TextBox'
import Loader from './Loader'

require('../../styles/group.scss')

@connect(store => {
	return {
		groups: store.groups,
        libraries: store.libraries,
        books: store.books,
		setting: store.setting,
	}
})
class Group extends React.Component {
    constructor() {
        super()
        this.state = {
            isLoading: true
        }
        this.handleExpandLibrary = this.handleExpandLibrary.bind(this)
    }
    handleExpandLibrary(state) {
        if(state) {
            const { dispatch, params } = this.props
            dispatch(fetchLibraryPage('/gruppenbibliothek/' + params.id + '/' + params.name + '/', 0))
        }
    }
    componentWillMount() {
        const { dispatch } = this.props
        dispatch(fetchGroup(this.props.params.id, this.props.params.name)).then(() => {
            this.state.isloading = false
        })
        this.state.isLoading = true
    }
    render() {
        let group = this.props.groups['/gruppe/' + this.props.params.id + '/' + this.props.params.name + '/']

        if (!group || !group.title) {
            return <Loader />
        }

        const created = formatLongDate(group.created)
        const lastupdated = new Date(group.lastupdated * 1000).toLocaleString('de', { day: 'numeric', year: 'numeric', month: 'long', hour: 'numeric', minute: 'numeric' })
        const library = this.props.libraries['/gruppenbibliothek/' + this.props.params.id + '/' + this.props.params.name + '/']

        let bookList = []
        let blackboard = []
        let memberList = []
        let hottestPosts = []
        let newComments = []
        this.state.isLoading = false

        if(library) {
            bookList = library.books.map(bookid => this.props.books[bookid])
        }

        if(group.notes) {
            blackboard = group.notes.map((note, index) => <div key={'note-' + index}>
                <Divider />
                <Avatar src={note.userimage} style={{ float: 'left', margin: '0 0.5rem 0.5rem 0' }} size={50} />
                <h3 class="inline">von {note.username}<br />{note.ago}</h3>
                <Divider style={{clear: 'left'}} />
                <TextBox text={note.content} />
            </div>)
        }

        return (
            <section class="group">
                <h2>{group.title}</h2>
                <h3>seit {created}, {group.members} Mitglieder</h3>
                <Avatar src={group.cover} size={100} style={{ float: 'left', margin: '0 1rem 1rem 0' }}/>
                <TextBox text={group.description} />
                <Divider style={{ clear: 'left' }} />
                <Card initiallyExpanded={true}>
                    <CardHeader title="Schwarzes Brett" actAsExpander={true} showExpandableButton={true} />
                    <CardText expandable={true}>
                        {blackboard}
                    </CardText>
                </Card>
                <Card>
                    <CardHeader title="Aktuelle Themen" actAsExpander={true} showExpandableButton={true} />
                    <CardText expandable={true}>
                        <Threads posts={group.posts.hottest} />
                    </CardText>
                </Card>
                <Card>
                    <CardHeader title="Neue Kommentare" actAsExpander={true} showExpandableButton={true} />
                    <CardText expandable={true}>
                        <Threads posts={group.posts.newest} />
                    </CardText>
                </Card>
                <Card onExpandChange={this.handleExpandLibrary}>
                    <CardHeader title="Bibliothek" actAsExpander={true} showExpandableButton={true} />
                    <CardText expandable={true}>
                        <BookList id={group.id} books={bookList} size="s" />
                    </CardText>
                </Card>
            </section>
        )

    }
}

export default withRouter(Group)
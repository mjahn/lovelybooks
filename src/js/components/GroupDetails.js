import React from 'react'
import Link from 'react-router/lib/Link'

require('../../styles/groupdetails.scss')

class GroupDetails extends React.Component {
    render() {
    	const group = this.props.group

        if(group === undefined) {
            return null
        }

        const created = new Date(group.created * 1000).toLocaleString('de', { day: 'numeric', year: 'numeric', month: 'long' })
        const lastupdated = new Date(group.lastupdated * 1000).toLocaleString('de', { day: 'numeric', year: 'numeric', month: 'long', hour: 'numeric', minute: 'numeric' })

        return (
        	<div class="groupdetails">
                <img class="groupdetails-cover" src={group.cover} />
                <h3 class="groupdetails-title"><Link to={group.link.split('/gruppe/').join('/group/')}>{group.title}</Link></h3>
                <ul class="groupdetails-details">
                    <li>Gründung: {created}</li>
                    <li>Mitglieder: {group.members}</li>
                </ul>
                <div class="groupdetails-description" dangerouslySetInnerHTML={{__html: group.description}}></div>
            </div>
      )

    }
}

export default GroupDetails
import React from 'react'
import { connect } from "react-redux"
import Quagga from 'quagga'

import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'

import Barcode from '../svg/barcode'

@connect(store => {
	return {
		unread: store.unread,
	}
})
class IsbnSearch extends React.Component {
	constructor() {
		super()
		this.state = {
			open: false,
			scanner_active: false,
			results_active: false,
		}
		this.handleOpen = this.handleOpen.bind(this)
		this.handleClose = this.handleClose.bind(this)
		this.handleDetect = this.handleDetect.bind(this)
		this.startScanner = this.startScanner.bind(this)
		this.stopScanner = this.stopScanner.bind(this)
	}
	handleOpen() {
		this.setState({open: true})
	}
	handleClose() {
		this.setState({open: false})
	}
	handleDetect(result) {
		this.state.isbn = result.codeResult.code
	}
	startScanner() {
		Quagga.init({
			inputStream : {
				width: '100%',
				height: 'auto',
				type : 'LiveStream',
				facingMode: 'environment',
				target: document.querySelector('.scanner-view')
			},
			decoder : {
				readers : ['code_128_reader']
			},
			debug: {
				drawBoundingBox: true,
				showFrequency: false,
				drawScanline: true,
				showPattern: true,
			},
			multiple: false
		}, function(err) {
			if (err) {
				console.log(err)
				return
			}
			Quagga.start()
		})
		Quagga.onDetected(this.handleDetect)
	}
	stopScanner() {

	}
	render() {
		const actions = [
			<FlatButton label="Cancel" primary={true} onTouchTap={this.handleClose} />,
		]
		return <div>
			<div onClick={this.handleOpen}><Barcode size={36} /></div>
			<Dialog title="ISBN-Suche" actions={actions} modal={false} open={this.state.open} onRequestClose={this.handleClose} autoScrollBodyContent={true}>
				<div>
					<TextField type="text" name="q" defaultValue={this.state.search} />
					<button type="button" onClick={this.startScanner}>Start</button>
				</div>
				<div class="scanner-view" style={{ width: '100%', margin: '1rem auto' }}></div>
				<div class="results-view">{this.state.isbn}</div>
			</Dialog>
		</div>
	}
}

export default IsbnSearch
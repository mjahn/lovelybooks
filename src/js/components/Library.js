import React from 'react'
import { connect } from "react-redux"
import withRouter from 'react-router/lib/withRouter'
import { fetchLibraryPage }  from '../actions/library'
import ExpandMore from 'material-ui/svg-icons/navigation/expand-more'

import BookList from './BookList'
import Loader from './Loader'

require('../../styles/library.scss')

@connect(store => {
	return {
		libraries: store.libraries,
		setting: store.setting,
	}
})
class Library extends React.Component {
    constructor() {
        super()
        this.state = {
			isLoading: true,
			libraryLoaded: 0,
		}
	}
	componentWillMount() {
		const { dispatch } = this.props
		dispatch(fetchLibraryPage(this.props.params.library, 0))
	}
	extendLibrary() {
		this.props.dispatch(fetchLibraryPage(key, this.state.libraryLoaded + 1)).then(response => {
			this.state.libraryLoaded++
		})
	}

    render() {
		const id = this.props.params.library
		const library = this.props.libraries[id]
		if (!library || !library.name) {
			return <Loader />
		}
		let bookList = []
		if(library.books) {
			bookList = library.books.map( bookid => this.props.books[bookid] )
		}

        return (
            <section class="library">
            	<h2>{library.name}</h2>
				<BookList id={library.id} books={bookList} size="m" />
            	<div class="library-extend" onClick={this.extendLibrary}>
            		<ExpandMore />
            	</div>
            </section>
        )

    }
}

export default withRouter(Library)
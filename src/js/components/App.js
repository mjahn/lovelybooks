import React from 'react'
import { connect } from "react-redux"
import withRouter from 'react-router/lib/withRouter'
import { checkUser, fetchUserData, logoutUser }  from '../actions/user'
import Link from 'react-router/lib/Link'

import FontIcon from 'material-ui/FontIcon'
import { BottomNavigation, BottomNavigationItem } from 'material-ui/BottomNavigation'
import AppBar from 'material-ui/AppBar'
import Drawer from 'material-ui/Drawer'
import Paper from 'material-ui/Paper'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import Divider from 'material-ui/Divider'

import { deepOrange900, brown800 } from 'material-ui/styles/colors';

import UserBadge from './UserBadge'
import NotificationArea from './NotificationArea'

@connect(store => {
	return {
		user: store.user,
		session: store.session,
		setting: store.setting,
	}
})
class App extends React.Component {
	constructor() {
		super()
		this.state = {
			docked: true,
			open: false
		}
		this.navigate = this.navigate.bind(this)
		this.toggleMenu = this.toggleMenu.bind(this)
		this.logout = this.logout.bind(this)
		this.back = this.back.bind(this)
	}
 	navigate(event, item, value) {
 		if(item.props && item.props.value === 'logout') {
 			this.logout()
	 		return
 		}
 		if(item.props && item.props.value) {
	 		this.props.router.push(item.props.value)
	 		return
 		}
 		this.setState( { open: false })
 	}
 	componentWillMount() {
 	    const { dispatch, location } = this.props
 	    
		if(location.pathname !== '/login') {
     	    dispatch(checkUser()).then(response => {
     	        console.log(response);
     	        if(response.payload.data.data.user.loggedIn) {
     	            return dispatch(fetchUserData(response.payload.data.data.user.name))
     	        }
     	    })
		}
 	}
	shouldComponentUpdate(nextProps) {
		const { user, session, router, location } = nextProps

		if(!this.props.session.isLoaded) {
			return false
		}

		if(location.pathname !== '/login') {
			if(!session.id || session.id === 'null' || !user.loggedIn) {
				if(location.pathname.indexOf('/login') === 0) {
					router.push('/login')
				} else {
					router.push('/login' + location.pathname)
				}
			}
		}
		return true
	}
	toggleMenu() {
		this.setState( { open: !this.state.open, docked: this.state.docked })
	}

	logout() {
		const { dispatch } = this.props
		console.dir(dispatch(logoutUser()))
	}

	back() {
		this.props.router.goBack()
	}
	render() {
		const { user } = this.props
		return <div class="app">
			<AppBar title="Lovelybooks Mobile" onLeftIconButtonTouchTap={this.toggleMenu} zDepth={2} iconElementRight={<NotificationArea />} />
			<Drawer docked={false} open={this.state.open} onRequestChange={open => this.setState({open})} zDepth={2}>
				<UserBadge user={user} onItemTouchTap={this.navigate} />
				<Divider />
				<Menu onItemTouchTap={this.navigate}>
					<MenuItem key="menu-dashboard" value="/dashboard" disabled={!user.loggedIn}>Übersicht</MenuItem>
					<MenuItem key="menu-library" value="/library" disabled={!user.loggedIn}>Bibliothek</MenuItem>
					<MenuItem key="menu-groups" value="/groups" disabled={!user.loggedIn}>Gruppen</MenuItem>
					<MenuItem key="menu-messages" value="/messages" disabled={!user.loggedIn}>Nachrichten</MenuItem>
					<Divider />
					<MenuItem key="menu-setiings" value="settings" disabled={!user.loggedIn}>Einstellungen</MenuItem>
					<MenuItem key="menu-logout" value="logout" disabled={!user.loggedIn}>Abmelden</MenuItem>
				</Menu>
			</Drawer>
			<main>
				{this.props.children}
			</main>
		</div>
	}
}


export default withRouter(App)
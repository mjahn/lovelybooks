import React from 'react'
import Avatar from 'material-ui/Avatar';
import Link from 'react-router/lib/Link'

class UserList extends React.Component {
    constructor() {
        super()
        this.state = {
        }
    }
    render() {
        let { users, id, size } = this.props

        if(!users) {
            users = []
        }
        if(!size) {
            size = '80'
        }

        let userList = users.map((user, index) => {
            const userlink = '/user/' + user.name + '/profile'
            return (
                <Link to={userlink}>
                    <Avatar size={size} src={user.image} />
                    {user.name}
                </Link>
            );
        })
        return (
    		{userList}
      	)

    }
}

export default UserList
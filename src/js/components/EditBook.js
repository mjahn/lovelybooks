import React from 'react'
import { connect } from "react-redux"
import withRouter from 'react-router/lib/withRouter'
import { fetchBook }  from '../actions/library'
import Link from 'react-router/lib/Link'

import FlatButton from 'material-ui/FlatButton'

import Loader from './Loader'

require('../../styles/book.scss')

@connect(store => {
	return {
		books: store.books,
		setting: store.setting,
	}
})
class EditBook extends React.Component {
    constructor() {
        super()
        this.state = {
        }
    }
    getBookId( { author, title } ) {
        return '/autor/' + author + '/' + title + '/'
    }
    componentWillMount() {
        const { dispatch } = this.props
        dispatch(fetchBook({author: this.props.params.author, title: this.props.params.title}))
    }
    render() {
        let book = this.props.books[this.getBookId(this.props.params)]
        return (
            <section class="book">
            </section>
        )

    }
}

export default withRouter(EditBook)
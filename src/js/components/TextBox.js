import React from 'react'
import { transformLinks, formatSentences } from '../formatter/text'
import Hypher from 'hypher'
import german from 'hyphenation.de'

require('../../styles/textbox.scss')

export default class TextBox extends React.Component {
    render() {
        let { text } = this.props
        let textData1 = ''

        if(!text) {
            return null
        }
        
        text = formatSentences(transformLinks(text))

        const hypher = new Hypher(german)
        text = hypher.hyphenateText(text)

        return (
            <div class="textbox" dangerouslySetInnerHTML={{__html: text}}></div>
        )

    }
}
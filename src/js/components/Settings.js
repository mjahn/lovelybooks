import React from 'react'
import ReactDOM from 'react-dom'
import { connect } from "react-redux"

import RaisedButton from 'material-ui/RaisedButton'

@connect(store => {
	return {
	}
})
class Settings extends React.Component {
    constructor() {
        super()
        this.resetState = this.resetState.bind(this)
    }
    resetState() {
        const { dispatch } = this.props
        dispatch({
            type: 'EMPTY',
            payload: { blacklist: ['user']}
        })
    }
    render() {
        return (
            <section class="dashboard">
                Wirklich alle lokalen Daten zurücksetzen?
                <RaisedButton secondary={true} onClick={this.resetState}>Ja, ich will</RaisedButton>
            </section>
        )

    }
}

export default Settings
import React from 'react'
import { connect } from "react-redux"
import withRouter from 'react-router/lib/withRouter'
import Link from 'react-router/lib/Link'

import { fetchForum, fetchThread }  from '../actions/forums'
import BookDetails from './BookDetails'
import Loader from './Loader'

import RaisedButton from 'material-ui/RaisedButton'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import Subheader from 'material-ui/Subheader'
import Avatar from 'material-ui/Avatar'
import Paper from 'material-ui/Paper'
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';

import PostList from './PostList'

@connect(store => {
	return {
		books: store.books,
        threads: store.threads,
        forums: store.forums,
		setting: store.setting,
	}
})
class Raffle extends React.Component {
    constructor() {
        super()
        this.state = {
            view: 'overview',
            threadLoaded: {},
        }
    }
    getBookId( { author, title } ) {
        return '/autor/' + author + '/' + title + '/'
    }
    componentWillMount() {
        const { dispatch } = this.props
        dispatch(fetchForum('/autor/' + this.props.params.author + '/' + this.props.params.title + '/leserunde/' + this.props.params.raffle + '/'))
    }
    render() {
        const book = this.props.books[this.getBookId(this.props.params)]
        const forum = this.props.forums['/autor/' + this.props.params.author + '/' + this.props.params.title + '/leserunde/' + this.props.params.raffle + '/']

        if (!book || !book.title || !forum || !forum.title) {
            return <Loader />
        }

        let threadList = forum.threads.map((id, index) => {
            const thread = this.props.threads[id]

            this['showThread' + index] = status => {
                if (status && !this.state.threadLoaded[index]) {
                    this.props.dispatch(fetchThread(thread.link)).then(response => {
                        this.state.threadLoaded[index] = 1
                    })
                }
            }

            return <Card key={'card-thread-' + thread.id} onExpandChange={this['showThread' + index]}>
                <CardHeader title={thread.title} actAsExpander={true} showExpandableButton={true} />
                <CardText expandable={true}>
                    <PostList posts={thread.posts} />
                </CardText>
            </Card>
        })

        return (
            <section class="forum">
                <h2>{forum.title}</h2>
                {threadList}
            </section>
        )

    }
}

export default withRouter(Raffle)
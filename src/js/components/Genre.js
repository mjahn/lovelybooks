import React from 'react'
import { connect } from "react-redux"
import withRouter from 'react-router/lib/withRouter'
import Link from 'react-router/lib/Link'
import BookList from './BookList'

import Loader from './Loader'

require('../../styles/group.scss')

@connect(store => {
	return {
		libraries: store.libraries,
        books: store.books,
		setting: store.setting,
	}
})
class Genre extends React.Component {
    constructor() {
        super()
    }
    componentWillMount() {
        const { dispatch } = this.props
        dispatch(fetchLibrary(this.props.params.id, this.props.params.name)).then(() => {
            this.state.isloading = false
        })
        this.state.isLoading = true
    }
    render() {
        const id = this.props.params.genre
        const library = this.props.libraries[id]
        if (!library || !library.name) {
            return <Loader />
        }
        let bookList = []
        if(library.books) {
            bookList = library.books.map( bookid => this.props.books[bookid] )
        }

        return (
            <section class="genre">
                <h2>{library.name}</h2>
                <BookList id={library.id} books={bookList} size="m" />
                <div class="library-extend" onClick={this.extendLibrary}>
                    <ExpandMore />
                </div>
            </section>
        )
    }
}

export default withRouter(Genre)
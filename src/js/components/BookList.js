import React from 'react'
import Link from 'react-router/lib/Link'

import Loader from './Loader'

require('../../styles/booklist.scss')

class BookList extends React.Component {
    constructor() {
        super()
        this.state = {
            listMode: 'grid'
        }
    }
    render() {
        let { books, id, size, raffles } = this.props

        if(!books) {
            books = []
        }
        if(!size) {
            size = 'm'
        }

        let bookList = books.map((book, index) => {
            if(!book || !book.authorlink || !book.author || !book.title || !book.cover) {
                return <Loader />
            }

            const booklink = book.link.split('/autor/').join('/library/book/')
            let bookcover = book.cover.split('_')
            bookcover.pop()
            bookcover = bookcover.join('_') + '_' + size.toLowerCase() + '.jpg'

            let raffle = []
            if(book.raffle) {
                raffle.push(<span key={book.raffle.id + 'amount'} className="booklist-raffle-amount">{book.raffle.amount} Bücher</span>)
                raffle.push(<span key={book.raffle.id + 'date'} className="booklist-raffle-date">endet am {book.raffle.date}</span>)
            }
            return (
                <div key={id + '-' + index} class="booklist-entry" title={book.title + ' von ' + book.author}>
                    <Link to={booklink} class="booklist-entry-link">
                        <img src={bookcover} class="booklist-entry-image" />
                        <div class="booklist-entry-text">
                            <strong>{book.title}</strong>
                            <em>{book.author}</em>
                        </div>
                    </Link>
                    {raffle}
                </div>
            );
        })
        return (
            <div class={'booklist booklist--' + size}>
        		{bookList}
            </div>
      	)
    }
}

export default BookList
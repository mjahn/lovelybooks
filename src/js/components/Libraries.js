import React from 'react'
import { connect } from "react-redux"
import withRouter from 'react-router/lib/withRouter'
import { fetchLibraries, fetchLibraryPage }  from '../actions/library'
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card'
import Link from 'react-router/lib/Link'
import ExpandMore from 'material-ui/svg-icons/navigation/expand-more'

import BookList from './BookList'

require('../../styles/libraries.scss')

@connect(store => {
	return {
		books: store.books,
		libraries: store.libraries,
		setting: store.setting,
	}
})
class Libraries extends React.Component {
    constructor() {
        super()
        this.state = {
			isLoading: true,
			libraryLoaded: {},
		}
	}
	componentWillMount() {
		const { dispatch } = this.props
		this.state.isLoading = true
		dispatch(fetchLibraries())
	}

	showLibrary(status) {
		console.dir(arguments)
	}
    render() {
		let { libraries, books } = this.props

    	if(!libraries) {
    		libraries = {}
    	}

		const libraryList = Object.keys(libraries).map((key, index) => {
			if (!libraries[key] || !libraries[key].name) {
				return null
			}

			const library = libraries[key]
			const amount = library.name.split('(')[1].split(')')[0]
			let bookList = []

			if(library.books) {
				bookList = library.books.map( bookid => books[bookid] )
			}

			this['showLibrary' + index] = function(status) {
				if (status && !this.state.libraryLoaded[key]) {
					this.props.dispatch(fetchLibraryPage(key, 0)).then(response => {
						this.state.libraryLoaded[key] = 0
					})
				}
			}.bind(this)
			this['extendLibrary' + index] = function() {
				if (this.state.libraryLoaded[key] >= 0) {
					if(library.books.length >= amount) {
						return
					}
					this.props.dispatch(fetchLibraryPage(key, this.state.libraryLoaded[key] + 1)).then(response => {
						this.state.libraryLoaded[key]++
					})
				}
			}.bind(this)

			return <Card key={'library-card-' + index} onExpandChange={this['showLibrary' + index]}>
                <CardHeader title={library.name.split('(')[0]} subtitle={amount + ' Bücher'} actAsExpander={true} showExpandableButton={true}>
                </CardHeader>
                <CardText expandable={true} style={{ overflow: 'hidden' }}>
					<BookList id={library.id} books={bookList} size="m" />
                	<div class="libraries-extend" onClick={this['extendLibrary' + index]} style={{ display: (bookList.length >= amount ? 'none' : 'block')}}>
                		<ExpandMore />
                	</div>
                </CardText>
            </Card>
		})

		this.state.isLoading = false

        return (
            <section class="libraries">
            	<h2>Meine Bibliothek</h2>
        		{libraryList}
            </section>
      )

    }
}

export default withRouter(Libraries)
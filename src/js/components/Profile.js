
import React from 'react'
import { connect } from "react-redux"
import withRouter from 'react-router/lib/withRouter'

import { fetchUserData }  from '../actions/user'
import Loader from './Loader'

import Avatar from 'material-ui/Avatar'
import IconButton from 'material-ui/IconButton'
import Message from 'material-ui/svg-icons/communication/message'
import PersonAdd from 'material-ui/svg-icons/social/person-add'

require('../../styles/profile.scss')

@connect(store => {
	return {
		books: store.books,
		user: store.user,
		users: store.users,
		setting: store.setting,
	}
})
class Profile extends React.Component {
    constructor() {
        super()
        this.state = {
			isLoading: true
		}
	}
	componentWillMount() {
		const { dispatch } = this.props
		if(this.props.params && this.props.params.user) {
			dispatch(fetchUserData(this.props.params.user)).then(response => { this.state.isLoading = false; })
		} else {
			dispatch(fetchUserData(this.props.user.name)).then(response => { this.state.isLoading = false; })
		}
	}

    render() {
    	const { user } = this.props
    	if (!user) {
    		return <Loader />
    	}
        if(!user.stats) {
            user.stats = {
                since: '',
                friends: 0,
                books: 0,
                eselsohren: 0,
                groups: 0,
                ratings: 0,
                ratingsAverage: 0,
                reviews: 0
            }
        }
        return (
            <section class="profile">
            	<h2>{user.name}</h2>
            	<Avatar class="profile-image" src={user.image} size={80} />
            	<ul class="profile-stats">
            		<li>dabei seit {user.stats.since}</li>
            		<li>{user.stats.friends} Freunde</li>
            		<li>{user.stats.books} Bücher</li>
            		<li>{user.stats.eselsohren} Eselsohren</li>
            		<li>Mitglied in {user.stats.groups} Gruppen</li>
            		<li>{user.stats.ratings} Bewertungen (&median; {user.stats.ratingsAverage})</li>
            		<li>{user.stats.reviews} Rezensionen</li>
            	</ul>
                <div class="profile-actions">
                    <IconButton href={'#/messages/new/' + user.name}><Message /></IconButton>
                    <IconButton href={'#/user/friends/' + user.name}><PersonAdd /></IconButton>
                </div>
            </section>
      	)

    }
}

export default withRouter(Profile)
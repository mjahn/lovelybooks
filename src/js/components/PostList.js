import React from 'react'
import Link from 'react-router/lib/Link'

import Avatar from 'material-ui/Avatar'
import { List, ListItem } from 'material-ui/List'

import TextBox from './TextBox'

class PostList extends React.Component {
    constructor() {
        super()
        this.state = {
            sortBy: 'id'
        }
    }
    render() {
        let posts = this.props.posts
        let postList = []
        posts.sort((a, b) => b[this.state.sortBy] - a[this.state.sortBy] )

        posts = posts.map((post, index) => <ListItem key={'post-' + post.id}>
            <div style={{ overflow: 'hidden' }}>
                <Avatar src={post.userimage} style={{ float: 'left', margin: '0.5rem' }} />
                {post.username}<br />
                {post.ago} 
            </div>
            <TextBox text={post.content} />
        </ListItem>)

        return (
            <section class="posts">
                <List>
                    {posts}
                </List>
	        </section>
      	)
    }
}

export default PostList
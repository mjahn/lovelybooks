import React from 'react'
import withRouter from 'react-router/lib/withRouter'

import Subheader from 'material-ui/Subheader';
import Link from 'react-router/lib/Link'
import {List, ListItem} from 'material-ui/List'
import Divider from 'material-ui/Divider'
import Avatar from 'material-ui/Avatar'
import IconButton from 'material-ui/IconButton'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import IconMenu from 'material-ui/IconMenu'
import MenuItem from 'material-ui/MenuItem'
import TextBox from './TextBox'
import { formatShortDateTime, formatMonthDate } from '../formatter/date'

require('../../styles/booklist.scss')

class MessageList extends React.Component {
    constructor() {
        super()
        this.state = {
            listMode: 'grid'
        }
        this.handleMessageMenu = this.handleMessageMenu.bind(this)
        this.showMessage = this.showMessage.bind(this)
    }
    handleMessageMenu(event, item, value) {
        this.props.router.push('/messages/new/' + item.props.value + '/' + item.props.dataMessageId)
    }
    showMessage(message) {
        return function() {
            this.props.router.push('/messages/' + message.folder + '/' + message.id)
        }.bind(this)
    }
    render() {
        let { messages } = this.props
        let oldDate = ''
        let messageList = []
        const iconButtonElement = (
        <IconButton touch={true} tooltip="more" tooltipPosition="bottom-left">
            <MoreVertIcon color="#981217" />
        </IconButton>
        )
        messages.sort((a, b) => b.date - a.date )

        messages.forEach(message => {
            const rightIconMenu = (
            <IconMenu iconButtonElement={iconButtonElement} onItemTouchTap={this.handleMessageMenu}>
                <MenuItem key="1" dataMessageId={message.id} value="reply">Antworten</MenuItem>
                <MenuItem key="3" dataMessageId={message.id} value="delete">Löschen</MenuItem>
            </IconMenu>
            )
            const newDate = formatMonthDate(message.date)
            if (oldDate !== newDate) {
                oldDate = newDate
                messageList.push(<Subheader key={'message-' + message.id + '-date'}>{newDate}</Subheader>)
            }
            let text = ''
            if(message.folder === 'inbox') {
                text = 'von ' + message.sendername + ' um ' + formatShortDateTime(message.date) + ': ' + message.description
            }
            if(message.folder === 'outbox') {
                text = 'an ' + message.sendername + ' um ' + formatShortDateTime(message.date) + ': ' + message.description
            }
            text = text.replace(/<\/?[^>]>/g, '')
            messageList.push(<ListItem innerDivStyle={{background: '#efece5'}} onClick={this.showMessage(message)} key={'message-' + message.id} leftAvatar={<Avatar src={message.senderimage} />} primaryText={message.subject} secondaryText={text} secondaryTextLines={2} rightIconButton={rightIconMenu} />)
            messageList.push(<Divider key={'message-' + message.id + '-divider'} inset={true} style={{ textAlign: 'center' }} />)
        })

        return (
            <List>
                {messageList}
            </List>
      	)

    }
}

export default withRouter(MessageList)
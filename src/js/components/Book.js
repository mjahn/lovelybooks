import React from 'react'
import { connect } from "react-redux"
import withRouter from 'react-router/lib/withRouter'
import { fetchBook }  from '../actions/library'
import BookDetails from './BookDetails'
import BookStatusBadge from './BookStatusBadge'
import Link from 'react-router/lib/Link'

import FlatButton from 'material-ui/FlatButton'
import Chip from 'material-ui/Chip'
import Avatar from 'material-ui/Avatar'
import { List, ListItem } from 'material-ui/List'
import { Card, CardActions, CardHeader, CardText } from 'material-ui/Card'

import Loader from './Loader'
import TextBox from './TextBox'
import { formatLongDate } from '../formatter/date'

require('../../styles/book.scss')

@connect(store => {
	return {
		books: store.books,
		setting: store.setting,
	}
})
class Book extends React.Component {
    constructor() {
        super()
        this.state = {
            isLoading: true
        }
    }
    getBookId( { author, title } ) {
        return '/autor/' + author + '/' + title + '/'
    }
    componentWillMount() {
        const { dispatch } = this.props
        dispatch(fetchBook({author: this.props.params.author, title: this.props.params.title})).then(() => {
            this.state.isloading = false
        })
        this.state.isLoading = true
    }
    render() {
        let book = this.props.books[this.getBookId(this.props.params)]
        if (!book || !book.title) {
            return <Loader />
        }
        this.state.isLoading = false
        let tags = null
        if(book.tags) {
            tags = book.tags.map((tag, index) => {
                return <Link key={'book-tags-' + index} to={'/library/tag/' + tag + '/'}>
                    <Chip>
                        {tag}
                    </Chip>
                </Link>
            })
            tags = <Card>
                <CardHeader title="Tags" actAsExpander={true} showExpandableButton={true} />
                <CardText expandable={true} class="book-tags">
                    {tags}
                </CardText>
            </Card>
        }
        let additions = null
        if(book.additions) {
            additions = book.additions.map((addition, index) => {
                if(addition.type !== 'rezension') {
                    return null
                }
                return <ListItem key={'book-addition-' + index}>
                    <div style={{ overflow: 'hidden' }}>
                        <Avatar src={addition.userimage} style={{ float: 'left', margin: '0.5rem' }} />
                        {addition.username}<br />
                        {formatLongDate(addition.published)} 
                    </div>
                    <h3><Link to={addition.link}>{addition.title}</Link></h3>
                    <TextBox text={addition.content} />
                </ListItem>
            })
            additions = <Card>
                <CardHeader title="Rezensionen" actAsExpander={true} showExpandableButton={true} />
                <CardText expandable={true} class="book-reviews">
                    <List>
                        {additions}
                    </List>
                </CardText>
            </Card>
        }
        let actions = []
        if(book.actions) {
            actions = book.actions.map(action => {
                if(action === 'Rezension schreiben') {
                    return <FlatButton key={'book-action-review'} href={'#' + book.link.replace('/autor/', '/library/book/') + 'review/'} primary={true}>Rezension schreiben</FlatButton>
                }
                if(action === 'In Bibliothek stellen') {
                    return <FlatButton key={'book-action-add'} href={'#' + book.link.replace('/autor/', '/library/book/') + 'add'} primary={true}>Ins Bücherregal stellen</FlatButton>
                }
                if(action === 'In Bibliothek bearbeiten') {
                    return <FlatButton key={'book-action-edit'} href={'#' + book.link.replace('/autor/', '/library/book/') + 'edit'} primary={true}>Im Bücherregal bearbeiten</FlatButton>
                }
                if(action === 'Dem Buch folgen') {
                    return <FlatButton key={'book-action-follow'} href={'#' + book.link.replace('/autor/', '/libray/book/') + 'follow'} primary={true}>Dem Buch folgen</FlatButton>
                }
                if(action === 'Du folgst diesem Buch') {
                    return <FlatButton key={'book-action-unfollow'} href={'#' + book.link.replace('/autor/', '/libray/book/') + 'unfollow'} primary={true}>Dem Buch nicht mehr folgen</FlatButton>
                }
            })
        }
        return (
            <section class="book">
                <BookDetails book={book} />
                <div style={{ textAlign: 'center', margin: '1rem 0' }}>
                    {actions}
                </div>
                <Card initiallyExpanded={true}>
                    <CardHeader title="Inhaltsangabe" actAsExpander={true} showExpandableButton={true} />
                    <CardText expandable={true}>
                        <TextBox text={book.description} />
                    </CardText>
                </Card>
                {additions}
                {tags}
            </section>
        )

    }
}

export default withRouter(Book)
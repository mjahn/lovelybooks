import React from 'react'
import { connect } from "react-redux"
import { loginUser, checkUser, fetchUserData }  from '../actions/user'
import withRouter from 'react-router/lib/withRouter'

import RaisedButton from 'material-ui/RaisedButton'
import Toggle from 'material-ui/Toggle'
import TextField from 'material-ui/TextField'


require('../../styles/login.scss')

@connect()
class Login extends React.Component {
    constructor() {
        super()
        this.state = {
            email: '',
            password: '',
            remember: 'on',
            error: '',
            isLoading: false
        }
        this.doLogin = this.doLogin.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }
    doLogin() {
        const { dispatch } = this.props
        this.state.isLoading = true
        dispatch(loginUser(this.state.email, this.state.password, this.state.remember === 'on')).then(response => {
            if(response.error) {
                this.state.isLoading = false
                this.state.password = ''
                this.state.error = 'Login ungültig'
                return Promise.reject();
            }
            this.state.error = ''
            return dispatch(checkUser())
        }).then(response => {
            return dispatch(fetchUserData(response.payload.data.data.user.name))
        }).then(response => {
            this.state.isLoading = false
            if(this.props.params.return) {
                this.props.router.push('/' + this.props.params.return)
            } else {
                this.props.router.push('/dashboard')
            }
        })
        return false
    }

    handleChange(event) {
        this.state[event.target.name] = event.target.value
        return true
    }

    render() {
        return (
            <section className="login">
                <h2>Login</h2>
                <div class="loginform">
                    <div>
                        <TextField underlineStyle={{ borderColor: '#433924' }} onChange={this.handleChange} hintText="Emailadresse eingeben" type="text" name="email" fullWidth={true} />
                        <TextField underlineStyle={{ borderColor: '#433924' }} onChange={this.handleChange} hintText="Passwort eingeben" type="password" name="password" fullWidth={true} />
                        <Toggle label="Angemeldet bleiben" name="remember" defaultValue={this.state.remember} onToggle={this.handleChange} />
                    </div>
                    <div>
                        <RaisedButton label="Einloggen" secondary={true} onClick={this.doLogin} style={{ marginTop: '1rem' }} fullWidth={true}  />
                    </div>
                </div>
            </section>
        )

    }
}

export default withRouter(Login)
export default function(state = {}, action) {
	switch (action.type) {
		case 'EMPTY':
			state = {}
			break;
		case 'FETCH_USER_SUCCESS':
			return Object.assign(
				{},
				state,
				{ [action.payload.data.data.user.id]: action.payload.data.data.user }
			)
			break
	}
	return state
}
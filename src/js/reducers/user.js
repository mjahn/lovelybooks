import { REHYDRATE } from 'redux-persist/constants'

export default function(state = {
	id: 0,
	name: '',
	loggedIn: false,
}, action) {
	switch (action.type) {
		case 'EMPTY':
			state = {
				id: 0,
				name: '',
				loggedIn: false,
			}
			break;
/*
		case REHYDRATE:
			return state;
			break;
*/
		case 'CHECK_USER_SUCCESS':
			if(!action.payload.data.data.user.loggedIn) {
				localStorage.removeItem('sessionId');
			}
			return Object.assign(
				{},
				state,
				{
					loggedIn: action.payload.data.data.user.loggedIn,
					name:  action.payload.data.data.user.name,
				}
			)
			break
		case 'FETCH_USER_SUCCESS':
			return Object.assign(
				{},
				state,
				action.payload.data.data.user
			)
			break
		case 'LOGOUT_USER_SUCCESS':
			return Object.assign(
				{},
				state,
				action.payload.data.data.user
			)
			break
		case 'LOGIN_USER_SUCCESS':
			return Object.assign(
				{},
				state,
				{
					loggedIn: action.payload.data.data.user.loggedIn,
					name:  action.payload.data.data.user.name,
				}
			)
			break
	}
	return state
}
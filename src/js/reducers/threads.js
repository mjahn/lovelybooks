export default function(state = {}, action) {
	switch (action.type) {
		case 'EMPTY':
			state = {}
			break;
		case 'FETCH_THREAD_SUCCESS':
			return Object.assign(
				{},
				state,
				{ [action.payload.data.data.thread.id]: Object.assign(
					{},
					state[action.payload.data.data.thread.id],
					action.payload.data.data.thread
				)},
			);
			break;
		default:
			if(action.payload && action.payload.data && action.payload.data.data && action.payload.data.data.threads) {
				return Object.assign(
					{},
					state,
					action.payload.data.data.threads
				)
			}
			break;
	}
	return state
}
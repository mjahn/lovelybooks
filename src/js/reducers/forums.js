export default function(state = {}, action) {
	switch (action.type) {
		case 'EMPTY':
			state = {}
			break;
		case 'FETCH_FORUM_SUCCESS': 
			let forum = {}
			forum = action.payload.data.data.forum

			state = Object.assign(
				{},
				state,
				{ [forum.id]: forum }
			)
			break;
	}
	return state
}
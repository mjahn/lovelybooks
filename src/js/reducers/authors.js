export default function(state = {}, action) {
	switch (action.type) {
		case 'EMPTY':
			state = {}
			break;
		case 'FETCH_AUTHOR_SUCCESS':
			return Object.assign(
				{},
				state,
				{ [ action.payload.data.data.author.link]: action.payload.data.data.author }
			);
			break;
	}
	return state
}
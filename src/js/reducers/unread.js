export default function(state = {
	messages: 0,
}, action) {
	switch (action.type) {
		case 'EMPTY':
			state = {
				messages: 0,
			}
			break;
		case 'LOGIN_USER_SUCCESS':
			return Object.assign(
				{},
				state,
				{ messages: (action.payload.data.data.user.unreadMessages ? 1 : 0) }
			)
			break

			break;
		case 'FETCH_MESSAGES_SUCCESS':
			let messageCounter = 0
			Object.keys(action.payload.data.data.messages).forEach(key => {
				messageCounter += (action.payload.data.data.messages[key].read === false ? 1 : 0)
			})
			return Object.assign(
				{},
				state,
				{ messages: messageCounter }
			)
			break
	}
	return state
}
export default function(state = {}, action) {
	switch (action.type) {
		case 'EMPTY':
			state = {}
			break;
		case 'FETCH_LIBRARIES_SUCCESS':
			Object.keys(action.payload.data.data.libraries).forEach(id => {
				action.payload.data.data.libraries[id].isLoading = false
				if(state[id] && state[id].books) {
					action.payload.data.data.libraries[id].books = state[id].books
				}
			})
			return Object.assign(
				{},
				state,
				action.payload.data.data.libraries
			);
			break;
		case 'FETCH_LIBRARY_PAGE_SUCCESS':
			let library = {}
			if(state[action.payload.data.data.library.id]) {
				library = Object.assign(
					{},
					state[action.payload.data.data.library.id]
				)
				library.isLoading = false
				library.id = action.payload.data.data.library.id

				library.books = library.books.concat(action.payload.data.data.library.books)
				library.books = library.books.reduce((p, c) => {
		        	if (p.indexOf(c) < 0) {
		        		p.push(c)
		        	}
		        	return p
		    	}, [])
			} else {
				library = action.payload.data.data.library
			}
			state = Object.assign(
				{},
				state,
				{ [library.id]: library }
			)
			return state
			break;

	}
	return state
}
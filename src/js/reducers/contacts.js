export default function(state = [], action) {
	switch (action.type) {
		case 'EMPTY':
			state = []
			break;
		case 'FETCH_MESSAGES_SUCCESS':
			state = state.concat(
				Object.keys(action.payload.data.data.messages).map(key => action.payload.data.data.messages[key].sender)
			)
			state = state.reduce((p, c) => {
	        	if (p.indexOf(c) < 0) {
	        		p.push(c);
	        	}
	        	return p;
	    	}, []);
			break
	}
	return state
}
export default function(state = {}, action) {
	switch (action.type) {
		case 'EMPTY':
			state = {}
			break;
		case 'FETCH_MESSAGES_SUCCESS':
			return Object.assign(
				{},
				state,
				action.payload.data.data.messages
			);
			break
	}
	return state
}
export default function(state = [], action) {
	switch (action.type) {
		case 'EMPTY':
			state = []
			break;
		case 'FETCH_MESSAGES_SUCCESS':
			if(action.meta.previousAction.payload.folder === 'inbox') {
				state = state.concat(
					Object.keys(action.payload.data.data.messages)
				)
				state = state.reduce((p, c) => {
		        	if (p.indexOf(c) < 0) {
		        		p.push(c);
		        	}
		        	return p;
		    	}, []);
			}
			break
	}
	return state
}
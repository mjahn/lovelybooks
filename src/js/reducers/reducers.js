import { combineReducers } from 'redux'
import authors from './authors'
import books from './books'
import bookstatus from './bookstatus'
import contacts from './contacts'
import forums from './forums'
import groups from './groups'
import inbox from './inbox'
import libraries from './libraries'
import messages from './messages'
import outbox from './outbox'
import publishers from './publishers'
import raffles from './raffles'
import session from './session'
import setting from './setting'
import tags from './tags'
import threads from './threads'
import unread from './unread'
import user from './user'
import users from './users'

export default combineReducers({
	authors,
	books,
	bookstatus,
	contacts,
	groups,
	forums,
	inbox,
	libraries,
	messages,
	outbox,
	publishers,
	raffles,
	session,
	setting,
	tags,
	threads,
	unread,
	user,
	users,
})
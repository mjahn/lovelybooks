export default function(state = {}, action) {
	switch (action.type) {
		case 'EMPTY':
			state = {}
			break;
		case 'UPDATE_BOOK_STATUS':
			if(state[action.payload.bookstatus] === undefined || state[action.payload.bookstatus] === null) {
				return state
			}
			const data = Object.assign(
				{},
				state[action.payload.bookstatus]
			)

			data.comments.unshift({
				comment: action.payload.data.comment,
				created: (new Date()).getTime() / 1000,
				pageCurrent: action.payload.data.page || state[action.payload.bookstatus].page,
				pageTotal: state[action.payload.bookstatus].pageTotal,
				percent: (state[action.payload.bookstatus].pageTotal == 0 ? 0 : (action.payload.data.page || state[action.payload.bookstatus].page || 0) * 100 / (state[action.payload.bookstatus].pageTotal))
			})
			state = Object.assign(
				{},
				state,
				{ [action.payload.bookstatus]: data }
			);
			console.log()
			break;
		case 'FETCH_BOOK_STATUS_SUCCESS':
			state = Object.assign(
				{},
				state,
				{ [action.payload.data.data.bookstatus.id]: action.payload.data.data.bookstatus }
			);
			break;
	}
	return state
}
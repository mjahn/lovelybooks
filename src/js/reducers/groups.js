export default function(state = {}, action) {
	switch (action.type) {
		case 'EMPTY':
			state = {}
			break;
		case 'FETCH_GROUP_SUCCESS':
			return Object.assign(
				{},
				state,
				{ [ action.payload.data.data.group.link ] : action.payload.data.data.group }
			);
			break;
		case 'FETCH_GROUPS_SUCCESS':
			return Object.assign(
				{},
				state,
				action.payload.data.data.groups
			);
			break;
	}
	return state
}
export default function(state = {}, action) {
	switch (action.type) {
		case 'EMPTY':
			state = {}
			break;
		case 'FETCH_BOOK_SUCCESS':
			return Object.assign(
				{},
				state,
				{ [action.payload.data.data.book.id]: Object.assign(
					{},
					state[action.payload.data.data.book.id],
					action.payload.data.data.book
				)},
			);
			break;
		default:
			if(action.payload && action.payload.data && action.payload.data.data && action.payload.data.data.books) {
				return Object.assign(
					{},
					state,
					action.payload.data.data.books
				)
			}
			break;
	}
	return state
}
export default function(state = {}, action) {
	switch (action.type) {
		case 'FETCH_PUBLISHER_SUCCESS':
			return Object.assign(
				{},
				state,
				{ [ action.payload.data.data.publisher.name ]: action.payload.data.data.publisher }
			);
			break;
	}
	return state
}
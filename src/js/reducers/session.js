import { REHYDRATE } from 'redux-persist/constants'

export default function(state = {
	id: '',
	isLoaded: false,
}, action) {
	switch (action.type) {
		case 'EMPTY':
			state = {
				id: '',
				isLoaded: true
			}
			break;
		case REHYDRATE:
			state = Object.assign(
				{},
				state,
				action.payload.session,
				{ isLoaded: true }
			)
			break;
		case 'LOGOUT_USER_SUCCESS':
			state = {
				id: '',
				isLoaded: true
			}
			localStorage.removeItem('sessionId')
			break;
		default:
			if(action.payload && action.payload.data && action.payload.data.sessionid) {
				state = {
					isLoaded: true,
					id: action.payload.data.sessionid
				}
				localStorage.setItem('sessionId', state.id)
			}
	}
	return state
}
export default function(state = {}, action) {
	switch (action.type) {
		case 'EMPTY':
			state = {}
			break;
		case 'FETCH_TAG_PAGE_SUCCESS':
			let tag = {}
			if(state[action.payload.data.data.tag.id]) {
				tag = Object.assign(
					{},
					state[action.payload.data.data.tag.id]
				)
				tag.id = action.payload.data.data.tag.id

				tag.books = tag.books.concat(action.payload.data.data.tag.books)
				tag.books = tag.books.reduce((p, c) => {
		        	if (p.indexOf(c) < 0) {
		        		p.push(c)
		        	}
		        	return p
		    	}, [])
			} else {
				tag = action.payload.data.data.tag
			}
			state = Object.assign(
				{},
				state,
				{ [tag.id]: tag }
			)
			console.log(state)
			break;

	}
	return state
}
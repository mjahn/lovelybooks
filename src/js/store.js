import { applyMiddleware, createStore } from "redux"
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import promiseMiddleware from 'redux-promise'
import { persistStore, autoRehydrate } from 'redux-persist'
import { REHYDRATE } from 'redux-persist/constants'
import createActionBuffer from 'redux-action-buffer'
import createLogger from 'redux-logger'
import immutableTransform from 'redux-persist-transform-immutable'
import localforage from 'localforage'

import reducer from "./reducers/reducers"

const client = axios.create({
	baseURL: document.location.protocol + '//lovelybooks.martinjahn.org/api',
	responseType: 'json'
});


const store = createStore(
	reducer,
	applyMiddleware(
		axiosMiddleware(client),
		promiseMiddleware,
		createActionBuffer(REHYDRATE),
		createLogger(),
	),
	autoRehydrate()
)
persistStore(store, {
	storage: localforage,
	transforms: [immutableTransform()]
})

export default store
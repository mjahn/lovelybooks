import { serialize } from '../formatter/http'

export function loginUser (email, password, remember) {
    return {
        type: 'LOGIN_USER',
        payload: {
            request:{
                url: '/login',
                method: 'post',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: serialize({email, password, remember})
            }
        }
    }
}

export function checkUser () {
    return {
        type: 'CHECK_USER',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/user',
            }
        }
    }
}
export function logoutUser () {
    return {
        type: 'LOGOUT_USER',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/logout',
            }
        }
    }
}

export function fetchUserData (name) {
    return {
        type: 'FETCH_USER',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/user/' + name,
            }
        }
    }
}

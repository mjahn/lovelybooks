import { serialize } from '../formatter/http'

export function fetchMessages(folder, page) {
    return {
        type: 'FETCH_MESSAGES',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/messages/' + folder + '/' + page,
                method: 'get',
            },
            folder
        }
    }
}
export function createMessage(receipients, subject, message) {
    const data = {
        receipients,
        subject,
        message
    }
    return {
        type: 'POST_MESSAGE',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/messages',
                method: 'post',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: serialize(data)
            }
        }
    }
}
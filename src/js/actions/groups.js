import { serialize } from '../formatter/http'

export function fetchGroups() {
    return {
        type: 'FETCH_GROUPS',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/groups',
                method: 'get',
            }
        }
    }
}
export function fetchGroup(id, name) {
    return {
        type: 'FETCH_GROUP',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/group/' + id + '/' + name,
                method: 'get',
            }
        }
    }
}
export function fetchGroupLibrary(id, name, page) {
    return {
        type: 'FETCH_GROUP_LIBRARY_PAGE',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/group/library/' + id + '/' + name + '/' + page,
                method: 'get',
            }
        }
    }
}

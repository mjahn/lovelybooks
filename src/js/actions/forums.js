import { serialize } from '../formatter/http'

export function fetchForum(url) {
    return {
        type: 'FETCH_FORUM',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/forum' + url,
                method: 'get',
            }
        }
    }
}

export function fetchThread(url) {
    url = url.split('/?tag=').join('/tag/')
    return {
        type: 'FETCH_THREAD',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/thread' + url,
                method: 'get',
            }
        }
    }
}

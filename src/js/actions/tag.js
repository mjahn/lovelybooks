export function fetchTagPage(tag, page) {
    return {
        type: 'FETCH_TAG_PAGE',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/tag/' + tag + '/' + page,
                method: 'get',
            },
            page,
            tag
        }
    }
}
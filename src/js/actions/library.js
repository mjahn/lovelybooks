import { serialize } from '../formatter/http'

export function fetchRaffles() {
    return {
        type: 'FETCH_RAFFLES',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/raffles',
                method: 'get',
            }
        }
    }
}

export function fetchLibraryPage(library, page) {
    return {
        type: 'FETCH_LIBRARY_PAGE',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/library' + library + page,
                method: 'get',
            },
            page,
            library
        }
    }
}

export function fetchLibraries() {
    return {
        type: 'FETCH_LIBRARIES',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/libraries',
                method: 'get',
            }
        }
    }
}

export function fetchBookStatus( { status } ) {
    return {
        type: 'FETCH_BOOK_STATUS',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/book/status/' + status,
                method: 'get',
            }
        }
    }
}

export function fetchBook( { author, title } ) {
    return {
        type: 'FETCH_BOOK',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/book/' + author + '/' + title,
                method: 'get',
            }
        }
    }
}

export function fetchAuthor(author) {
    return {
        type: 'FETCH_AUTHOR',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/author/' + author,
                method: 'get',
            }
        }
    }
}

export function fetchPublisher(publisher) {
    return {
        type: 'FETCH_PUBLISHER',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/publisher/' + publisher,
                method: 'get',
            }
        }
    }
}

export function updateBookStatus(bookstatus, data) {
    return {
        type: 'UPDATE_BOOK_STATUS',
        payload: {
            request:{
                url: '/' + localStorage.getItem('sessionId') + '/book/status/' + bookstatus,
                method: 'post',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: serialize(data)
            },
            bookstatus,
            data
        }
    }
}
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from "react-redux"
import Router from 'react-router/lib/Router'
import Route from 'react-router/lib/Route'
import IndexRoute from 'react-router/lib/IndexRoute'
import hashHistory from 'react-router/lib/hashHistory'
import injectTapEventPlugin from 'react-tap-event-plugin'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import store from './store'
import theme from './theme'

import About from './components/About'
import App from './components/App'
import Author from './components/Author'
import Book from './components/Book'
import BookStatus from './components/BookStatus'
import Dashboard from './components/Dashboard'
import EditBook from './components/EditBook'
import Genre from './components/Genre'
import Group from './components/Group'
import GroupForum from './components/GroupForum'
import Groups from './components/Groups'
import Raffle from './components/Raffle'
import Libraries from './components/Libraries'
import Library from './components/Library'
import Login from './components/Login'
import Message from './components/Message'
import MessageForm from './components/MessageForm'
import Messages from './components/Messages'
import Profile from './components/Profile'
import Settings from './components/Settings'
import Tag from './components/Tag'

require('../styles/index.scss')

injectTapEventPlugin()
const metaElement = document.createElement('meta')
metaElement.setAttribute('name', 'viewport');
metaElement.setAttribute('content', 'width=device-width, initial-scale=1, maximum-scale=1');
document.head.appendChild(metaElement)

const element = document.createElement('div')
element.setAttribute('id', 'root')
document.body.appendChild(element)

const muiTheme = getMuiTheme(theme)

ReactDOM.render((
<Provider store={store}>
 	<MuiThemeProvider muiTheme={muiTheme}>
	 	<Router history={hashHistory}>
			<Route path="/" component={App}>
				<IndexRoute component={Dashboard} />
				<Route path="/dashboard" component={Dashboard} />
				<Route path="/settings" component={Settings} />
				<Route path="/login(/:return)" component={Login} />
				<Route path="/library" component={Libraries} />
				<Route path="/library/genre/:genre/" component={Genre} />
				<Route path="/library/tag/:tag/" component={Tag} />
				<Route path="/library/author/:author/" component={Author} />
				<Route path="/library/book/:author/:title/" component={Book} />
				<Route path="/library/book/:author/:title/:action" component={EditBook} />
				<Route path="/library/book/:author/:title/status/:status" component={BookStatus} />
				<Route path="/library/book/:author/:title/raffle/:raffle" component={Raffle} />
				<Route path="/library(/:library)" component={Library} />
				<Route path="/user/(:user/)profile" component={Profile} />
				<Route path="/messages/new" component={MessageForm} />
				<Route path="/messages/new/:action/:id" component={MessageForm} />
				<Route path="/messages/new/:to" component={MessageForm} />
				<Route path="/messages/:folder/:id" component={Message} />
				<Route path="/messages" component={Messages} />
				<Route path="/groups/:id/:name" component={Group} />
				<Route path="/groups/:id/:name/themen/:thread" component={GroupForum} />
				<Route path="/groups" component={Groups} />
				<Route path="*" component={Dashboard} />
			</Route>
		</Router>
	 </MuiThemeProvider>
</Provider>
), element)